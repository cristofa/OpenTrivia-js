FROM node:12

WORKDIR /app
COPY package* ./
RUN npm i --only=production
COPY build/. ./

ENV NODE_ENV production
EXPOSE 8080

CMD [ "node", "server.js" ]
