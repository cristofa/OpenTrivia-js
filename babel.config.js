module.exports = {
  presets: [
    [
      '@babel/preset-env',
      {
        targets: {
          node: 'current',
        },
      },
    ],
    '@babel/preset-react',
  ],
  plugins: ['@babel/plugin-proposal-class-properties', '@loadable/babel-plugin', 'react-hot-loader/babel'],
  ignore: ['node_modules', 'build'],
};
