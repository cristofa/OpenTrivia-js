import React from 'react';
import { Typography, Box } from '@material-ui/core';

import styles from './css/NewGame.module.css';
import TopBar from '../components/common/TopBar';
import CreateGame from '../components/newGame/CreateGame';

const NewGame = () => (
  <React.Fragment>
    <TopBar />
    <Box m={3}>
      <Typography variant="h5" align="center">Welcome to OpenTrivia.eu</Typography>
    </Box>
    <Typography className={styles.infoText} align="center">To start playing configure and create new game. You will get a link you can share with friends to play together.</Typography>
    <Box className={styles.container}>
      <CreateGame />
    </Box>
  </React.Fragment>
);

export default NewGame;
