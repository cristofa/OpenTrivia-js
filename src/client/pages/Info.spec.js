import { describe, expect } from '@jest/globals';
import React from 'react';
import { render, screen } from '@testing-library/react';

import Info from './Info';
import addRouter from '../utils/addRouter';


describe('Info page', () => {
  test('renders Info page', () => {
    render(addRouter(<Info/>));
    expect(screen.getByRole('button', {name: 'Info'})).toBeInTheDocument();
    expect(screen.getByRole('link', {name: 'OpenTrivia.eu'})).toBeInTheDocument();
    expect(screen.getByText(/Open Trivia Database/)).toBeInTheDocument();
    expect(screen.getByText('How it works')).toBeInTheDocument();
    expect(screen.getByText('It\'s Open Source, baby!')).toBeInTheDocument();
  });
});
