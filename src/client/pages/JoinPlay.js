import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { useSelector, useDispatch } from 'react-redux';

import { getWsUrl} from '../utils/getServerUrl';
import PlayerSetup from '../components/gamePlay/PlayerSetup';
import PlayStatus from '../components/common/PlayStatus';
import { wsConnect, wsJoinPlay, wsRejoinPlay, wsUpdateRequest } from '../features/websocketSlice';
import TopBar from '../components/common/TopBar';

export function JoinPlay(props) {

  const { params } = props.match;
  const status = useSelector(state => state.websocket.status);
  const playId = useSelector(state => state.websocket.playId);
  const playerId = useSelector(state => state.websocket.playerId);
  const dispatch = useDispatch();

  useEffect(() => {
    const storedPlayerId = window.localStorage.getItem(playId);
    if (status == PlayStatus.CONNECTING) {
      dispatch(wsConnect(getWsUrl(params.playId)));
    } else if (status == PlayStatus.CONNECTED) {
      if (storedPlayerId) {
        dispatch(wsRejoinPlay(storedPlayerId));
      } else {
        dispatch(wsJoinPlay());
      }
    } else if (status == PlayStatus.LOGGED_IN) {
      if (!storedPlayerId) {
        localStorage.setItem(playId, playerId);
      }
      dispatch(wsUpdateRequest(playerId));
    } else if (status == PlayStatus.SET_UP) {
      props.history.push('/play/' + params.playId);
    }
  });

  return (
    <React.Fragment>
      <TopBar />
      <PlayerSetup playId={params.playId} playerId={playerId}/>
    </React.Fragment>);
}


JoinPlay.propTypes = {
  match: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired
};

export default JoinPlay;
