import { describe, expect } from '@jest/globals';
import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import { createMemoryHistory } from 'history';
import { Router } from 'react-router-dom';

import ShareUrl from './ShareUrl';

const testUuid = 'test-uuid';
const testHost = 'http://host:888';
const joinPath = '/join/';

const getHistory = () => {
  const history = createMemoryHistory();
  history.push({
    pathname: '/share',
    data: {
      uuid: testUuid,
      host: testHost,
    }
  });
  return history;
};

describe('ShareUrl page', () => {
  test('renders ShareUrl page', () => {
    const history = getHistory();

    render(<Router history={history}><ShareUrl /></Router>);
    expect(screen.getByRole('button', {name: 'Info'})).toBeInTheDocument();
    expect(screen.getByRole('link', {name: 'OpenTrivia.eu'})).toBeInTheDocument();
    expect(screen.getByText('Share this link with other players.')).toBeInTheDocument();
    expect(screen.getByText('Once everyone has it you can join the game.')).toBeInTheDocument();
    expect(screen.getByRole('button', {name: 'join-game'})).toBeInTheDocument();
    expect(screen.getByRole('button', {name: 'copy-link'})).toBeInTheDocument();
    expect(screen.getByRole('textbox', {name: 'Game URL'})).toBeInTheDocument();

    expect(screen.getByRole('textbox', {name: 'Game URL'})).toHaveTextContent(testHost + joinPath + testUuid);
  });

  test('Join game', () => {

    const history = getHistory();
    render(<Router history={history}><ShareUrl /></Router>);
    history.push = jest.fn();

    fireEvent.click(screen.getByRole('button', {name: 'join-game'}));
    expect(history.push).toHaveBeenCalledWith(joinPath + testUuid);
  });

  test('Go to home page', () => {
    const history = getHistory();
    render(<Router history={history}><ShareUrl /></Router>);
    history.push = jest.fn();

    fireEvent.click(screen.getByRole('link', {name: 'OpenTrivia.eu'}));
    expect(history.push).toHaveBeenCalledWith('/');
  });

  test('Go to Info page', () => {

    const history = getHistory();
    render(<Router history={history}><ShareUrl /></Router>);
    history.push = jest.fn();

    fireEvent.click(screen.getByRole('link', {name: ''}));
    expect(history.push).toHaveBeenCalledWith('/info');
  });
});
