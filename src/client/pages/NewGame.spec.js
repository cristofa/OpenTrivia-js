import { describe, expect } from '@jest/globals';
import React from 'react';
import { render, screen } from '@testing-library/react';

import NewGame from './NewGame';
import addRouter from '../utils/addRouter';


describe('NewGame page', () => {
  test('renders NewGame page', () => {
    render(addRouter(<NewGame/>));
    expect(screen.getByText(/To start playing configure and create new game./)).toBeInTheDocument();
    expect(screen.getByRole('button', {name: 'Info'})).toBeInTheDocument();
    expect(screen.getByRole('link', {name: 'OpenTrivia.eu'})).toBeInTheDocument();
  });
});
