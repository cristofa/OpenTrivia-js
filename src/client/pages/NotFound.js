import React from 'react';
import { Typography, Box } from '@material-ui/core';
import SentimentVeryDissatisfiedIcon from '@material-ui/icons/SentimentVeryDissatisfied';
import Link from '@material-ui/core/Link';

import TopBar from '../components/common/TopBar';
import styles from './css/NotFound.module.css';

const NotFound = () => (
  <React.Fragment>
    <TopBar />
    <Box m={10}>
      <Typography variant="h4" align="center">You have found a secret page!</Typography>
    </Box>
    <Box m={2} className={styles.center}>
      <Typography>Unfortunately there is nothing here </Typography>
      <SentimentVeryDissatisfiedIcon />
    </Box>
    <Box m={2} className={styles.center}>
      <Typography>
        Start again from the
        <Link href="/">main page.</Link>
      </Typography>
    </Box>
  </React.Fragment>
);

export default NotFound;
