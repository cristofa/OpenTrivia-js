import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { useDispatch, useSelector} from 'react-redux';

import GameScreen from '../components/gamePlay/GameScreen';
import PlayStatus from '../components/common/PlayStatus';
import {wsConnect, wsJoinPlay, wsRejoinPlay, wsUpdateRequest} from '../features/websocketSlice';
import {getWsUrl} from '../utils/getServerUrl';

export function GamePlay(props) {

  const { params } = props.match;
  const status = useSelector(state => state.websocket.status);
  const playerId = useSelector(state => state.websocket.playerId);
  const playId = useSelector(state => state.websocket.playId);
  const dispatch = useDispatch();

  useEffect(() => {
    const storedPlayerId = window.localStorage.getItem(playId);
    if (status == PlayStatus.CONNECTING) {
      dispatch(wsConnect(getWsUrl(params.playId)));
    } else if (status == PlayStatus.CONNECTED) {
      if (storedPlayerId) {
        dispatch(wsRejoinPlay(storedPlayerId));
      } else {
        dispatch(wsJoinPlay());
      }
    } else if (status == PlayStatus.LOGGED_IN) {
      props.history.push('/join/' + params.playId);
    } else if (status == PlayStatus.SET_UP) {
      dispatch(wsUpdateRequest(playerId));
    }
  });

  return (<GameScreen playerId={playerId}/>);
}


GamePlay.propTypes = {
  match: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired
};
export default GamePlay;
