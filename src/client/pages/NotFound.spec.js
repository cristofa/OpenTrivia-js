import { describe, expect } from '@jest/globals';
import React from 'react';
import { render, screen } from '@testing-library/react';

import NotFound from './NotFound';
import addRouter from '../utils/addRouter';


describe('NotFound page', () => {
  test('renders NotFound page', () => {
    render(addRouter(<NotFound/>));
    expect(screen.getByRole('button', {name: 'Info'})).toBeInTheDocument();
    expect(screen.getByRole('link', {name: 'OpenTrivia.eu'})).toBeInTheDocument();
    expect(screen.getByText('You have found a secret page!')).toBeInTheDocument();
  });
});
