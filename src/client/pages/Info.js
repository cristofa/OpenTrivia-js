import React from 'react';
import { Typography, Box } from '@material-ui/core';
import HelpOutlineIcon from '@material-ui/icons/HelpOutline';
import CopyrightIcon from '@material-ui/icons/Copyright';
import Link from '@material-ui/core/Link';
import { makeStyles } from '@material-ui/core/styles';

import theme from '../theme';
import TopBar from '../components/common/TopBar';

const useStyles = makeStyles(theme => ({
  footer: {
    position: 'fixed',
    bottom: '0',
    left: '0',
    display: 'flex',
    alignItems: 'center',
    width: '98vw',
    marginTop: '25px',
    backgroundColor: theme.palette.primary.main,
  },
  copyright: {
    marginLeft: '10px',
  },
}));

const Info = () => {
  const classes = useStyles(theme);

  return (
    <React.Fragment>
      <TopBar />
      <div style={{ maxWidth: '1000px', margin: 'auto', marginBottom: '100px' }}>
        <Box m={3}>
          <Typography variant="h5" align="center">
            <HelpOutlineIcon fontVariant="h1" fontSize="large" />
            <br />
                        OpenTrivia.eu
                        is a free, multiplayer trivia game using questions from Open Trivia Database (
            <Link href="https://opentdb.com/">opentdb.com</Link>).
            {' '}
          </Typography>
        </Box>

        <Box mt={5}>
          <Typography variant="h5" align="center">How it works</Typography>
        </Box>
        <Box m={2}>
          <Typography align="center">
                        Simply create new game and share a link with your friends. By accessing it everyone
                        will be able to
                        join the same game and play against each other. You can set the number of questions and
                        choose categories and difficulty levels from which those will be selected. In each round players will
                        vote on the next category. One random question will be chosen from this category.
          </Typography>
        </Box>

        <Box mt={5}>
          {/* eslint-disable-next-line react/no-unescaped-entities */}
          <Typography variant="h5" align="center">It's Open Source, baby!</Typography>
        </Box>
        <Box m={2}>
          <Typography align="center">
                        You can find the entire code base on <Link href="https://gitlab.com/cristofa/OpenTrivia-js">gitlab</Link>.
                        Feel free to copy it, fork it or create a pull request.
          </Typography>
        </Box>
      </div>

      <Box className={classes.footer}>
        <CopyrightIcon className={classes.copyright} style={{ color: 'white' }} fontSize="small" />
        <Typography className={classes.copyright} style={{ color: 'white' }} variant="overline">  2020 - Krzysztof Andrelczyk</Typography>
      </Box>
    </React.Fragment>
  );
};

export default Info;
