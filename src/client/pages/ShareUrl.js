import React from 'react';
import { Typography, Box } from '@material-ui/core';
import FileCopyOutlinedIcon from '@material-ui/icons/FileCopyOutlined';
import PlayArrowIcon from '@material-ui/icons/PlayArrow';
import { makeStyles } from '@material-ui/core/styles';
import { withRouter } from 'react-router-dom';
import TextField from '@material-ui/core/TextField';
import Fab from '@material-ui/core/Fab';
import PropTypes from 'prop-types';

import theme from '../theme';
import TopBar from '../components/common/TopBar';

const useStyles = makeStyles(() => ({
  centered: {
    margin: 'auto',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  root: {
    '& label.Mui-focused': {
      color: theme.palette.primary.main,
    },
    '& .MuiInput-underline:after': {
      borderBottomColor: theme.palette.primary.main,
    },
    '& .MuiOutlinedInput-root': {
      '& fieldset': {
        borderColor: theme.palette.primary.main,
      },
      '&:hover fieldset': {
        borderColor: theme.palette.primary.main,
      },
      '&.Mui-focused fieldset': {
        borderColor: theme.palette.primary.main,
      },
    },
  },
  copyIcon: {
    margin: '5px',
  },
}));

const ShareUrl = (props) => {
  const classes = useStyles(theme);
  const joinPath = '/join/';

  if (!props.location.data) {
    props.history.push('/');
    return <div />;
  }

  const { uuid } = props.location.data;
  const { host } = props.location.data;
  const gameUrl = host + joinPath + uuid;

  const textFieldClicked = () => {
    const field = document.getElementById('game-url-input');
    field.select();
  };

  const copyLink = (e) => {
    e.preventDefault();
    textFieldClicked();
    document.execCommand('copy');
  };

  const joinGame = () => {
    props.history.push(joinPath + uuid);
  };

  return (
    <React.Fragment>
      <TopBar />
      <Box m={5}>
        <Typography variant="h4" align="center">Share this link with other players.</Typography>
      </Box>

      <Box m={2} className={classes.centered}>
        <form className={classes.root} noValidate>
          <TextField
            onClick={textFieldClicked}
            id="game-url-input"
            label="Game URL"
            size="medium"
            variant="outlined"
            multiline
            rows={3}
            defaultValue={gameUrl}
            InputProps={{
              readOnly: true,
            }}
          />
        </form>
        <Fab
          className={classes.copyIcon}
          aria-label='copy-link'
          onClick={copyLink}
          variant="extended"
          size="medium"
          color="primary"
        >
          <FileCopyOutlinedIcon />
          Copy link
        </Fab>
      </Box>
      <Box mt={15} mb={5}>
        <Typography variant="h5" align="center">Once everyone has it you can join the game.</Typography>
      </Box>
      <Box className={classes.centered}>
        <Fab
          aria-label='join-game'
          className={classes.copyIcon}
          onClick={joinGame}
          variant="extended"
          size="medium"
          color="secondary"
        >
          <PlayArrowIcon />
          Join game
        </Fab>
      </Box>
    </React.Fragment>
  );
};

ShareUrl.propTypes = {
  history: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired
};

export default withRouter(ShareUrl);
