import { createMuiTheme } from '@material-ui/core/styles';

let theme = createMuiTheme({
  palette: {
    background: {
      paper: '#e0e0e0',
      default: '#f5f5f5',
    },
    primary: {
      main: '#1565c0',
      contrastText: '#fff',
    },
    secondary: {
      main: '#ff8f00',
      contrastText: '#000',
    },
    error: {
      main: '#f34834',
      contrastText: '#000',
    },
    text: {
      primary: '#000',
      secondary: '#000',
    },
  },
  typography: {
    h6: {
      '@media (max-width:600px)': {
        fontSize: '0.75rem',
      },
    },
    h4: {
      '@media (max-width:400px)': {
        fontSize: '1.5rem',
      },
    }
  }
});

export default theme;
