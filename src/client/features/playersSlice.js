import { createSlice, createEntityAdapter, createSelector } from '@reduxjs/toolkit';

const playersAdapter = createEntityAdapter({
  selectId: (player) => player.playerId
});

const initialState = playersAdapter.getInitialState({
  ids: [],
  entities: {
  },
});

export const playersSlice = createSlice({
  name: 'players',
  initialState,
  reducers: {
    updatePlayers: (state, action) => {
      playersAdapter.setAll(state, action.payload.state.players);
    },
  },
});

export const { updatePlayers } = playersSlice.actions;

export const {
  selectAll: selectAllPlayers,
  selectById: selectPlayerById,
  selectIds: selectPlayersIds,
} = playersAdapter.getSelectors(state => state.players);

export const selectUsedAvatars = createSelector(
  selectAllPlayers,
  (players) => {
    const usedAvatars = [];
    for (const i in players) {
      if (players[i].avatar != 0 && !players[i].self) {
        usedAvatars.push(players[i].avatar);
      }
    }
    return usedAvatars;
  }
);

export const selectPlayersByOption = createSelector(
  selectAllPlayers,
  (_, selected) => selected,
  (players, selected) => players.filter(p => p.selected == selected)
);

export const selectPlayersByScore = createSelector(
  selectAllPlayers,
  (_, score) => score,
  (players, score) => players.filter(p => p.points == score)
);

export const selectOwnPlayer = createSelector(
  selectAllPlayers,
  (players) => {
    const self = players.filter(p => p.self);
    return self.length ? self[0] : null;
  }
);

export const selectCurrentAvatar = createSelector(
  selectOwnPlayer,
  (player) => player !== null ? player.avatar : 0
);
export default playersSlice.reducer;
