import { createSlice } from '@reduxjs/toolkit';

import PlayStatus from '../components/common/PlayStatus';


const initialState = { host: '', connected: null, status: PlayStatus.CONNECTING,  error: '' };

export const wsSlice = createSlice({
  name: 'websocket',
  initialState,
  reducers: {
    wsConnect(state) {
      return state;
    },
    wsUpdateRequest(state) {
      return state;
    },
    wsSelectAvatar(state) {
      return state;
    },
    wsSetName(state) {
      return state;
    },
    wsDisconnect(state) {
      return state;
    },
    wsConnected(state) {
      state.connected = true;
    },
    wsDisconnected(state) {
      state.connected = false;
    },
    wsStatus(state, action) {
      state.status = action.payload.status;
      if (action.payload.playId) {
        state.playId = action.payload.playId;
      }
      if (action.payload.playerId) {
        state.playerId = action.payload.playerId;
      }
    },
    wsError(state, action) {
      state.error = action.payload;
      state.status = PlayStatus.OFFLINE;
    },
    wsJoinPlay() {
    },
    wsSetReady() {
    },
    wsSelectOption() {
    },
    wsRejoinPlay() {
    },
    wsChatMessage() {
    }
  },
});

export const {
  wsConnect, wsDisconnect, wsConnected, wsDisconnected, wsStatus, wsError, wsJoinPlay, wsRejoinPlay, wsUpdateRequest,
  wsSelectAvatar, wsSetName, wsSetReady, wsSelectOption, wsChatMessage
} = wsSlice.actions;


export default wsSlice.reducer;
