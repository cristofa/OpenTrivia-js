import { createSlice } from '@reduxjs/toolkit';


const initialState = {
  time: null, question: null, options: [], selected: null, selectedAns: null, categoryVote: false,
  ended: false, maxScore: 0
};

export const wsSlice = createSlice({
  name: 'game',
  initialState,
  reducers: {
    setGameState(state, action) {
      const receivedState = action.payload.state;
      if (state.categoryVote != receivedState.categoryVote) {
        state.selected = null;
      }
      Object.assign(state, receivedState);
    },
    selectOption(state, action) {
      state.selected = action.payload.selected;
    }
  },
});

export const {
  setGameState,
  selectOption
} = wsSlice.actions;


export default wsSlice.reducer;
