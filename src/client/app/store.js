import { configureStore } from '@reduxjs/toolkit';

import playersReducer from '../features/playersSlice';
import wsReducer from '../features/websocketSlice';
import gameReducer from '../features/gameSlice';
import wsMiddleware from './wsMiddleware';

export default configureStore({
  reducer: {
    players: playersReducer,
    websocket: wsReducer,
    game: gameReducer
  },
  middleware: [wsMiddleware],
});
