import React from 'react';
import loadable from '@loadable/component';
import { Route, Switch } from 'react-router-dom';
import { Provider } from 'react-redux';
import { hot } from 'react-hot-loader/root';

import store from './store';
// eslint-disable-next-line no-unused-vars
import style from './App.module.css';

const GamePlay = loadable(() => import('../pages/GamePlay'));
const JoinPlay = loadable(() => import('../pages/JoinPlay'));
const NewGame = loadable(() => import('../pages/NewGame'));
const Info = loadable(() => import('../pages/Info'));
const ShareUrl = loadable(() => import('../pages/ShareUrl'));
const NotFound = loadable(() => import('../pages/NotFound'));


const App = () => (
  <Provider store={store}>
    <Switch>
      <Route exact path="/" component={NewGame} />
      <Route exact path="/game" component={GamePlay} />
      <Route exact path="/info" component={Info} />
      <Route exact path="/share" component={ShareUrl} />
      <Route exact path="/join/:playId" component={JoinPlay} />
      <Route exact path="/play/:playId" component={GamePlay} />
      <Route component={NotFound} />
    </Switch>
  </Provider>
);

export default hot(App);
