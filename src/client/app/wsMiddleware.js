import { wsConnected, wsDisconnected, wsDisconnect, wsStatus, wsError } from '../features/websocketSlice';
import { updatePlayers } from '../features/playersSlice';
import { setGameState, selectOption } from '../features/gameSlice';
import Messages from '../../common/Messages';
import PlayStatus from '../components/common/PlayStatus';
import send from '../../common/sendMessage';

const wsMiddleware = () => {
  let socket = null;

  const onOpen = (store) => () => {
    store.dispatch(wsConnected());
  };

  const onClose = store => () => {
    store.dispatch(wsDisconnected());
  };

  const onMessage = store => (event) => {
    const payload = JSON.parse(event.data);
    switch (payload.type) {
    case Messages.STATUS.type:
      switch (payload.message) {
      case Messages.STATUS.CONNECTED_TO_PLAY:
        store.dispatch(wsStatus({ status: PlayStatus.CONNECTED, playId: payload.data.playId }));
        break;
      case Messages.STATUS.JOINED_PLAY:
        store.dispatch(wsStatus({ status: PlayStatus.LOGGED_IN, playerId: payload.data.playerId }));
        break;
      case Messages.STATUS.SET_UP:
        store.dispatch(wsStatus({ status: PlayStatus.SET_UP, playerId: payload.data.playerId }));
        break;
      }
      break;
    case Messages.UPDATE.type:
      if (payload.message == Messages.UPDATE.PLAYERS_STATE) {
        store.dispatch(updatePlayers({state: payload.data}));
      } else if (payload.message == Messages.UPDATE.GAME_STATE) {
        store.dispatch(setGameState({state: payload.data}));
      } else if (payload.message == Messages.UPDATE.SELECTION) {
        store.dispatch(selectOption({selected: payload.data.selected}));
      }
      break;
    case Messages.CHAT.type:
      if (payload.message == Messages.CHAT.MESSAGE) {
        const elementId = `playerBox${payload.data.senderId}`;
        const event = new CustomEvent('showMessage', { detail: payload.data.message });
        document.getElementById(elementId).dispatchEvent(event);
      }
      break;
    case Messages.ERROR.type:
      switch (payload.message) {
      case Messages.ERROR.NOT_FOUND:
      case Messages.ERROR.PLAY_FULL:
      case Messages.ERROR.PLAY_ENDED:
        store.dispatch(wsError(payload.message), wsDisconnect());
        break;
      default:
        store.dispatch(wsError(payload.message));
        break;
      }
      break;
    default:
      break;
    }
  };

  return store => next => (action) => {
    switch (action.type) {
    case 'websocket/wsConnect':
      if (socket !== null) {
        socket.close();
      }

      socket = new WebSocket(action.payload);

      socket.onmessage = onMessage(store);
      socket.onclose = onClose(store);
      socket.onopen = onOpen(store, socket);

      break;
    case 'websocket/wsDisconnect':
      if (socket !== null) {
        socket.close();
      }
      socket = null;
      break;
    case 'websocket/wsJoinPlay':
      send(socket, Messages.JOIN_PLAY, Messages.JOIN_PLAY.JOIN);
      break;
    case 'websocket/wsRejoinPlay':
      send(socket, Messages.JOIN_PLAY, Messages.JOIN_PLAY.REJOIN, {playerId: action.payload});
      break;
    case 'websocket/wsUpdateRequest':
      send(socket, Messages.UPDATE, Messages.UPDATE.REQUEST, {playerId: action.payload});
      break;
    case 'websocket/wsSelectAvatar':
      send(socket, Messages.ACTION, Messages.ACTION.SELECT_AVATAR, action.payload);
      break;
    case 'websocket/wsSetName':
      send(socket, Messages.ACTION, Messages.ACTION.SET_NAME, action.payload);
      break;
    case 'websocket/wsSetReady':
      send(socket, Messages.ACTION, Messages.ACTION.SET_READY, action.payload);
      break;
    case 'websocket/wsSelectOption':
      send(socket, Messages.ACTION, Messages.ACTION.SELECT_OPTION, action.payload);
      break;
    case 'websocket/wsChatMessage':
      send(socket, Messages.ACTION, Messages.ACTION.CHAT_MESSAGE, action.payload);
      break;
    default:
      return next(action);
    }
  };
};

export default wsMiddleware();
