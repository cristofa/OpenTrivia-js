import config from '../../config';

const getPort = () => {
  return location.port.length > 0 ? `:${location.port}` : '';
};

const getHttpUrl = (path) => {
  return `${location.protocol}//${config.host}${getPort()}/${path}`;
};

const getWsUrl = (playId) => {
  return `${location.protocol == 'https:' ? 'wss' : 'ws'}://${config.host}${getPort()}/ws/play/${playId}`;
};


export { getHttpUrl, getWsUrl };
