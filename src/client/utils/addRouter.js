import React from 'react';
import { MemoryRouter } from 'react-router-dom';

const addRouter = (component) => {
  return <MemoryRouter initialEntries={[ '/share' ]}>{component}</MemoryRouter>;
};

export default addRouter;
