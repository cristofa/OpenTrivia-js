import React from 'react';
import {Provider} from 'react-redux';

const addProvider = (component, store) => {
  return <Provider store={store}>{component}</Provider>;
};

export default addProvider;
