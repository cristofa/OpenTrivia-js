import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import PropTypes from 'prop-types';
import {Typography} from '@material-ui/core';
import ErrorOutlineIcon from '@material-ui/icons/ErrorOutline';
import Box from '@material-ui/core/Box';

import ErrorMessages from './ErrorMessage';
import Messages from '../../../common/Messages';

const useStyles = makeStyles(() => ({
  title: {
    display: 'flex',
    alignItems: 'center'
  },
  icon: {
    marginRight: '10px'
  }
}));


const ErrorDialog = (props) => {
  const classes = useStyles();

  const { error, connected, serverError } = props;
  if (!error && connected !== false && !serverError) {
    return (<div aria-label='empty'></div>);
  }

  let errorMessage;
  if (error) {
    switch (error) {
    case Messages.ERROR.NOT_FOUND:
      errorMessage = ErrorMessages.NOT_FOUND;
      break;
    case Messages.ERROR.PLAY_FULL:
      errorMessage = ErrorMessages.PLAY_FULL;
      break;
    case Messages.ERROR.PLAY_ENDED:
      errorMessage = ErrorMessages.PLAY_ENDED;
      break;
    default:
      errorMessage = ErrorMessages.OTHER;
    }
  } else if (serverError) {
    errorMessage = ErrorMessages.ERROR_CREATING_GAME;
  } else if (connected === false) {
    errorMessage = ErrorMessages.DISCONNECTED;
  }

  return (
    <React.Fragment>
      <Dialog open={true}>
        <DialogTitle  disableTypography id="error-dialog-title">
          <Box className={classes.title}>
            <ErrorOutlineIcon className={classes.icon} color='error'/>
            <Typography  color='error' variant='h5'>{errorMessage.title}</Typography>
          </Box>
        </DialogTitle>
        <DialogContent dividers className={classes.dialog} >
          <Typography variant='h5'>{errorMessage.content}</Typography>
        </DialogContent>
      </Dialog>
    </React.Fragment>
  );
};

ErrorDialog.propTypes = {
  error: PropTypes.string,
  serverError: PropTypes.bool,
  connected: PropTypes.bool
};

export default ErrorDialog;
