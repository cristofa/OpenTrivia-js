import config from '../../../config';

const ErrorMessages = {
  DISCONNECTED: {
    title: 'Connection lost',
    content: 'Application lost connection to the server. Check your internet connection and try reloading the page.'
  },
  NOT_FOUND: {
    title: 'Game not found',
    content: 'We couldn\'t find a game with this identifier. Make sure your link is correct.'
  },
  PLAY_FULL: {
    title: 'No slots available',
    content: `${config.maxPlayers} players are already connected to this game.`
  },
  PLAY_ENDED: {
    title: 'Game finished',
    content: 'This game has already finished. Start a new one!'
  },
  ERROR_CREATING_GAME: {
    title: 'Error while creating game',
    content: 'There was un error while trying to create new game. Please try again later.'
  },
  OTHER: {
    title: 'Sorry, there was an error',
    content: 'Something unexpected happened and the game stopped. Please try playing again later.'
  }
};

export default ErrorMessages;

