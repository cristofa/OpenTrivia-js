const PlayStatus = {
  CONNECTING: 'connecting',
  OFFLINE: 'offline',
  CONNECTED: 'connected',
  LOGGED_IN: 'loggedIn',
  SET_UP: 'setUp'
};

export default PlayStatus;

