import { describe, expect } from '@jest/globals';
import React from 'react';
import { render, screen } from '@testing-library/react';

import TopBar from './TopBar';
import addRouter from '../../utils/addRouter';

describe('TopBar component', () => {

  test('renders TopBar component', () => {
    render(addRouter(<TopBar />));
    expect(screen.getByRole('banner')).toBeInTheDocument();
    expect(screen.getByRole('link', {name: 'OpenTrivia.eu'})).toBeInTheDocument();
    expect(screen.getByRole('button', {name: 'Info'})).toBeInTheDocument();
  });

});
