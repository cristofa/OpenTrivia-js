import { describe, expect } from '@jest/globals';
import React from 'react';
import { render, screen } from '@testing-library/react';
import configureStore from 'redux-mock-store';

import ErrorDialog from './ErrorDialog';
import addProvider from '../../utils/addProvider';
import ErrorMessages from './ErrorMessage';
import Messages from '../../../common/Messages';


const mockStore = configureStore();

let store;

beforeEach(() => {
  const initialState = {};
  store = mockStore(initialState);
});

describe('ErrorDialog component', () => {

  test('renders empty ErrorDialog component', () => {
    render(addProvider(<ErrorDialog />, store));
    expect(screen.getByLabelText('empty')).toBeInTheDocument();
  });

  test('renders offline ErrorDialog component', () => {
    render(addProvider(<ErrorDialog connected={false}/>, store));

    expect(screen.getByRole('dialog')).toBeInTheDocument();
    expect(screen.getByRole('heading', {name: ErrorMessages.DISCONNECTED.title})).toBeInTheDocument();
    expect(screen.getByRole('heading', {name: ErrorMessages.DISCONNECTED.content})).toBeInTheDocument();
  });

  test('renders server error ErrorDialog component', () => {
    render(addProvider(<ErrorDialog serverError={true}/>, store));

    expect(screen.getByRole('dialog')).toBeInTheDocument();
    expect(screen.getByRole('heading', {name: ErrorMessages.ERROR_CREATING_GAME.title})).toBeInTheDocument();
    expect(screen.getByRole('heading', {name: ErrorMessages.ERROR_CREATING_GAME.content})).toBeInTheDocument();
  });

  test('renders error ErrorDialog component', () => {
    render(addProvider(<ErrorDialog error={Messages.ERROR.PLAY_FULL}/>, store));

    expect(screen.getByRole('dialog')).toBeInTheDocument();
    expect(screen.getByRole('heading', {name: ErrorMessages.PLAY_FULL.title})).toBeInTheDocument();
    expect(screen.getByRole('heading', {name: ErrorMessages.PLAY_FULL.content})).toBeInTheDocument();
  });
});
