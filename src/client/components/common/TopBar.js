import React from 'react';
import Link from '@material-ui/core/Link';
import { makeStyles } from '@material-ui/core/styles';
import { Link as RouterLink } from 'react-router-dom';
import IconButton from '@material-ui/core/IconButton';
import InfoIcon from '@material-ui/icons/Info';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';

import theme from '../../theme';

const useStyles = makeStyles(() => ({
  title: {
    flexGrow: 1,
  },
}));

const TopBar = () => {
  const classes = useStyles(theme);

  return (
    <AppBar position="sticky">
      <Toolbar>
        <Link className={classes.title} component={RouterLink} variant="h4" color="inherit" to="/">OpenTrivia.eu</Link>
        <Link component={RouterLink} to="/info" variant="h3">
          <IconButton aria-label="Info">
            <InfoIcon style={{ color: 'white' }} />
          </IconButton>
        </Link>
      </Toolbar>
    </AppBar>
  );
};

export default TopBar;
