import React, {useEffect, useState} from 'react';
import PropTypes from 'prop-types';
import Paper from '@material-ui/core/Paper';
import { useSelector } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import CancelIcon from '@material-ui/icons/Cancel';
import {Typography} from '@material-ui/core';
import ReactHtmlParser from 'react-html-parser';

import {selectPlayerById} from '../../features/playersSlice';
import style from './css/GameScreen.module.css';
import theme from '../../theme';
import PlayerStatus from '../../../server/model/PlayerStatus';

const useStyles = makeStyles(() => ({
  image: {
    width: '100%',
    height: '90%',
    objectFit: 'contain',
    paddingTop: '5px',
    paddingLeft: '20px'
  },
  message: {
    objectFit: 'contain',
    paddingTop: '20px',
    paddingLeft: '20px',
    position: 'absolute',
    transformOrigin: '65% 65%',
    animation: '$msgAnimation 2000ms linear',
    fontSize: '40pt'
  },
  '@media (orientation: portrait)': {
    message: {
      fontSize: '25pt'
    }
  },
  '@keyframes msgAnimation': {
    '0%': {
      transform: 'scale(0) rotate(-260deg)'
    },
    '50%': {
      transform: 'scale(1.5) rotate(0deg)'
    },
    '100%': {
      transform: 'scale(0) rotate(260deg)'
    }
  },
  offline: {
    opacity: '50%'
  },
  online: {
    opacity: '100%'
  },
  name: {
    position: 'absolute',
    color: theme.palette.primary.main
  },
  points: {
    position: 'absolute',
    color: theme.palette.primary.dark,
    right: '5px'
  },
  status: {
    position: 'absolute',
    bottom: '0px',
    left: '5%'
  },
  selfBox: {
    backgroundColor: '#f3f3f3'
  }
}));

const PlayerBox = (props) => {
  const classes = useStyles();
  const elementId = `playerBox${props.playerId}`;
  const [message, setMessage] = useState(null);

  useEffect(() => {
    const element = document.getElementById(elementId);
    if (element) {
      element.addEventListener('showMessage', (e) => {
        setMessage(e.detail);
        setTimeout(() => {
          setMessage(null);
        }, 2000);
      });
    }
  });
  const player = useSelector((state) => selectPlayerById(state, props.playerId));

  if (player.name == '') {
    return null;
  }

  let statusIcon = player.status === PlayerStatus.ONLINE ?
    <CheckCircleIcon aria-label='online' style={{color: 'green'}}/> : <CancelIcon aria-label='offline' style={{color: 'red'}}/>;

  let statusClass = player.status === PlayerStatus.ONLINE ? classes.online : classes.offline;
  let selfClass = player.self ? classes.selfBox : '';
  return (
    <Paper id={elementId} className={`${style.player} ${statusClass} ${selfClass}`} elevation={ player.ready ? 7 : 0}>
      <Typography className={classes.name} variant='h6'><b>{player.name}</b></Typography>
      <Typography className={classes.points} variant='h4'>{player.points}</Typography>
      <span className={message ? classes.message : ''}>{ReactHtmlParser(message)}</span>

      <img className={classes.image} src={`/player${player.avatar}.png`}/>
      <Typography className={classes.status} variant='body1'>{statusIcon}</Typography>
    </Paper>
  );
};

PlayerBox.propTypes = {
  playerId: PropTypes.number.isRequired
};
export default PlayerBox;
