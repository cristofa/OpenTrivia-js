import React from 'react';
import { useSelector } from 'react-redux';
import Paper from '@material-ui/core/Paper';

import style from './css/GameScreen.module.css';
import { selectPlayersIds } from '../../features/playersSlice';
import PlayerBox from './PlayerBox';
import ErrorDialog from '../common/ErrorDialog';
import ReadyToggle from './ReadyToggle';
import TimeBox from './TimeBox';
import Questions from './Questions';
import Winner from './Winner';
import Chat from './Chat';
import QuestionCounter from './QuestionCounter';


const GameScreen = () => {
  let ids = useSelector(selectPlayersIds);
  const error = useSelector(state => state.websocket.error);
  const connected = useSelector(state => state.websocket.connected);
  const question = useSelector(state => state.game.question);
  const ended = useSelector(state => state.game.ended);

  let leftColumn = [];
  let rightColumn = [];

  for (let i = 0; i < ids.length; i++) {
    const box = <PlayerBox key={i} playerId={ids[i]}/>;
    if (i%2) {
      leftColumn.push(box);
    } else {
      rightColumn.push(box);
    }
  }

  let mainContent;
  if (ended) {
    mainContent = <Winner/>;
  } else {
    mainContent = question && question.length > 0 ? <Questions/> : <ReadyToggle/>;
  }

  return (<div className={style.container}>
    <div className={style.column}>
      {leftColumn}
    </div>
    <Paper id='mainContainer' elevation={6} className={style.centralColumn}>
      <div className={style.contentHeader}>
        <QuestionCounter/>
        <TimeBox/>
      </div>
      <div className={style.content}>
        {mainContent}
        <Chat/>
      </div>
    </Paper>
    <div className={style.column}>
      {rightColumn}
    </div>
    <ErrorDialog error={error} connected={connected}/>
  </div>);
};

export default GameScreen;
