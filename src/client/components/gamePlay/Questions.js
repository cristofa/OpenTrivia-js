import React, {useEffect} from 'react';
import { useSelector} from 'react-redux';
import { Typography } from '@material-ui/core';
import {makeStyles} from '@material-ui/core/styles';
import ReactHtmlParser from 'react-html-parser';

import OptionSelector from './OptionSelector';

const useStyles = makeStyles(() => ({
  question: {
    height: '5vh',
    width: '95%'
  },
  questionText: {
    marginBottom: '25px',
    width: '41vw'
  },
  '@media (orientation: portrait)': {
    questionText: {
      width: '80vw'
    }
  },
  normal: {
    color: '#087108'
  },
  ending: {
    color: '#f33d3d'
  },
  buttonsGroup: {
    display: 'flex',
    flexDirection: 'column',

  }
}));


const buttonsGroupId = 'buttonsGroup';
const questionDivId = 'questionDiv';
const ansButtonName = 'ansButton';

const adjustFontSize = () => {
  const container = document.getElementById('mainContainer');
  const buttonsGroup = document.getElementById(buttonsGroupId);
  const question = document.getElementById(questionDivId);

  if (container && buttonsGroup && question) {
    let baseFontSize = 23;
    let baseButtonSize = 12;
    do {
      baseFontSize--;
      question.style.fontSize = `${baseFontSize}pt`;
      if (baseFontSize < 18) {
        baseButtonSize--;
        if (baseButtonSize < 5) {
          return;
        }
      } else {
        baseButtonSize = 12;
      }
      document.getElementsByName(ansButtonName).forEach((b) => {
        b.style.fontSize = `${baseButtonSize}pt`;
      });
    } while (question.offsetTop + question.offsetHeight + buttonsGroup.offsetHeight+ 30 > container.offsetHeight);
  }
};


const Questions = () => {
  const classes = useStyles();

  const question = useSelector(state => state.game.question);
  const options = useSelector(state => state.game.options);
  const selectors = options.map((o, i) => {
    return (<OptionSelector key={i} ansButtonName={ansButtonName} index={i} option={`${o}`}/>);
  });

  const questionElement = <Typography id={questionDivId}
    className={classes.questionText}
    component='div'
    align='center'>{ReactHtmlParser(question)}</Typography>;

  adjustFontSize();
  useEffect(() => {
    window.addEventListener('resize', adjustFontSize);
    return () => window.removeEventListener('resize', adjustFontSize);
  });

  return (
    <div className={classes.question}>
      {questionElement}
      <div
        id={buttonsGroupId}
        className={classes.buttonsGroup}
      >
        {selectors}
      </div>
    </div>
  );
};

export default Questions;
