import React from 'react';
import { useSelector} from 'react-redux';
import { Typography } from '@material-ui/core';
import {makeStyles} from '@material-ui/core/styles';
import TimerIcon from '@material-ui/icons/Timer';
import Paper from '@material-ui/core/Paper';

const useStyles = makeStyles(() => ({
  timeBox: {
    height: '5vh',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    padding: '5px',
    width: '80px',
    marginLeft: '30px',
    justifyContent: 'center'
  },
  normal: {
    color: '#087108'
  },
  ending: {
    color: '#f33d3d'
  }
}));

const TimeBox = () => {
  const classes = useStyles();

  const time = useSelector(state => state.game.time);
  if (time == null) {
    return  <div className={classes.timeBox}/>;
  }
  return (
    <Paper className={classes.timeBox}>
      <div className={`${time > 3 ? classes.normal : classes.ending}`}>
        <Typography variant='h3'><TimerIcon/>{time}</Typography>
      </div>
    </Paper>
  );
};

export default TimeBox;
