import React, {useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {makeStyles} from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import TextField from '@material-ui/core/TextField';
import {Typography} from '@material-ui/core';
import Fab from '@material-ui/core/Fab';
import PlayArrowIcon from '@material-ui/icons/PlayArrow';

import ProgressOverlay from '../newGame/ProgressOverlay';
import AvatarSelector from './AvatarSelector';
import { selectCurrentAvatar, selectUsedAvatars, selectAllPlayers } from '../../features/playersSlice';
// eslint-disable-next-line no-unused-vars
import * as images from '../../assets/index';
import ErrorDialog from '../common/ErrorDialog';
import { wsSetName } from '../../features/websocketSlice';

const useStyles = makeStyles(() => ({
  container: {
    margin: '10px',
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'center'
  },
  centered: {
    display: 'flex',
    marginTop: '10px',
    justifyContent: 'space-around'
  },
  submit: {
    float: 'right',
    marginRight: '50px',
    marginBottom: '30px'
  }
}));

const PlayerSetup = (props) => {
  const classes = useStyles();

  const dispatch = useDispatch();
  const error = useSelector(state => state.websocket.error);
  const connected = useSelector(state => state.websocket.connected);

  const [name, setName] = useState('');
  const players = useSelector(selectAllPlayers);
  const usedAvatars = useSelector(selectUsedAvatars);
  const selected = useSelector(selectCurrentAvatar);
  const progress = players.length > 0 && connected ? <div></div> : <ProgressOverlay onClick={(e) => e.preventDefault()} />;

  const scrollToFab = () => {
    const element = document.querySelector('#join-game');
    element.scrollIntoView();
  };

  const nameChanged = (e) => {
    setName(e.target.value);
  };
  const handleSubmit = () => {
    dispatch(wsSetName({
      playerId: props.playerId,
      name: name
    }));
  };
  const avatars = [];
  let i = 1;
  for (let image in images) {
    avatars.push(<AvatarSelector callback={scrollToFab} playerId={props.playerId} selected={selected == i} used={usedAvatars.indexOf(i) >= 0}
      usedAvatars={usedAvatars} key={image} index={i}/>);
    i++;
  }

  return (<div>
    {progress}
    <div className={classes.centered}>
      <Typography align='center' variant='h4'>Set your player name and select an avatar.</Typography>
    </div>
    <div className={classes.centered}>
      <TextField
        onChange={nameChanged}
        id='player-nae'
        label='Player name'
        size='medium'
        variant='outlined'
        inputProps={{ maxLength: 8 }}
      />
    </div>
    <div className={classes.container}>
      {avatars}
    </div>
    <Fab id='join-game'
      className={classes.submit}
      disabled={name.length == 0 || selected == 0}
      onClick={handleSubmit}
      variant='extended'
      size='large'
      color='secondary'
      aria-label='join-game'
    >
      Join Game
      <PlayArrowIcon />
    </Fab>

    <ErrorDialog error={error} connected={connected}/>
  </div>);
};

PlayerSetup.propTypes = {
  playerId: PropTypes.string
};

export default PlayerSetup;
