import React from 'react';
import {useSelector} from 'react-redux';
import {Typography} from '@material-ui/core';
import Avatar from '@material-ui/core/Avatar';
import {makeStyles} from '@material-ui/core/styles';

import { selectPlayersByScore } from '../../features/playersSlice';
import style from './css/Winner.module.css';
import theme from '../../theme';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'center',
    '& > *': {
      marginTop: '30px',
    },
    maxWidth: '100%'
  },
  large: {
    width: theme.spacing(7),
    height: theme.spacing(7),
  },
  extraLarge: {
    width: theme.spacing(15),
    height: theme.spacing(15),
  },
}));
const Winner = () => {
  const classes = useStyles(theme);
  const maxScore = useSelector(state => state.game.maxScore);
  const players = useSelector((state) => selectPlayersByScore(state, maxScore));
  let heading;
  if (players.length > 1) {
    heading = 'It\'s a tie! The winners are:';
  } else {
    heading = 'And the winner is:';
  }

  const avatars = players.map(p => <Avatar key={p.playerId}
    className={`${style.avatar} ${players.length == 1 ? classes.extraLarge :classes.large}`}
    src={`/player${p.avatar}.png`}/>);

  return (
    <React.Fragment>
      <Typography align='center' variant='h3'>{heading}</Typography>
      <div className={classes.root}>
        {avatars}
      </div>
    </React.Fragment>
  );
};

export default Winner;
