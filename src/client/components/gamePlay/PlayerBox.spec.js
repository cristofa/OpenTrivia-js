import { describe, expect } from '@jest/globals';
import React from 'react';
import { render, screen } from '@testing-library/react';

import store from '../../app/store';
import PlayerBox from './PlayerBox';
import addProvider from '../../utils/addProvider';
import { updatePlayers } from '../../features/playersSlice';

const defaultName = 'mockName1';

const setPlayer = (player = {}) => {

  const defaultPlayer = {
    playerId:0,
    status:'online',
    name:defaultName,
    avatar:1,
    points:0,
    ready:true,
    selected:null,
    self:false
  };

  store.dispatch(updatePlayers({state: {
    players: [
      {...defaultPlayer, ...player}
    ]
  }}));
};

describe('PlayerBox component', () => {
  test('renders PlayerBox component', () => {
    setPlayer();
    render(addProvider(<PlayerBox playerId={0}/>, store));

    expect(screen.getByRole('heading', {name: defaultName})).toBeInTheDocument();
    expect(screen.getByRole('heading', {name: '0'})).toBeInTheDocument();
    expect(screen.getByRole('img')).toHaveAttribute('src', '/player1.png');
    expect(screen.getByLabelText('online')).toBeInTheDocument();

  });

  test('renders offline status', () => {
    const points = 10;
    setPlayer( { status: 'offline', points: points});
    render(addProvider(<PlayerBox playerId={0}/>, store));

    expect(screen.getByRole('heading', {name: defaultName})).toBeInTheDocument();
    expect(screen.getByRole('heading', {name: `${points}` })).toBeInTheDocument();
    expect(screen.getByRole('img')).toHaveAttribute('src', '/player1.png');
    expect(screen.getByLabelText('offline')).toBeInTheDocument();
  });
});
