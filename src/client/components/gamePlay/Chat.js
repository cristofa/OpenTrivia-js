import React, {useState} from 'react';
import ChatRoundedIcon from '@material-ui/icons/ChatRounded';
import CloseIcon from '@material-ui/icons/Close';
import {Button, Typography} from '@material-ui/core';
import DialogTitle from '@material-ui/core/DialogTitle';
import Box from '@material-ui/core/Box';
import DialogContent from '@material-ui/core/DialogContent';
import Dialog from '@material-ui/core/Dialog';
import {makeStyles} from '@material-ui/core/styles';
import {useDispatch, useSelector} from 'react-redux';
import IconButton from '@material-ui/core/IconButton';

import style from './css/Chat.module.css';
import {wsChatMessage} from '../../features/websocketSlice';


const useStyles = makeStyles(() => ({
  title: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  icon: {
    marginRight: '10px'
  },
  close: {
    float: 'right'
  },
  dialog: {
    minWidth: '150px'
  }
}));


const Chat = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const playerId = useSelector(state => state.websocket.playerId);
  const [open, setOpen] = useState(false);

  const toggleDialog = () => {
    setOpen(!open);
  };

  const sendMessage = (message) => {
    dispatch(wsChatMessage({ playerId, message}));
    toggleDialog();
  };

  return (
    <React.Fragment>

      <Dialog open={open}>
        <DialogTitle  disableTypography id="chat-dialog-title">
          <Button className={classes.close} onClick={toggleDialog}>
            <CloseIcon/>
          </Button>
          <Box className={classes.title}>
            <Typography  color='primary' variant='h5'>Send message</Typography>
          </Box>
        </DialogTitle>
        <DialogContent  dividers>
          <Button onClick={() => {sendMessage('&#128514;');}}>
            <Typography variant='h2'>&#128514;</Typography>
          </Button>
          <Button onClick={() => {sendMessage('&#128526;');}}>
            <Typography variant='h2'>&#128526;</Typography>
          </Button>
          <Button onClick={() => {sendMessage('&#128534;');}}>
            <Typography variant='h2'>&#128534;</Typography>
          </Button>
          <Button onClick={() => {sendMessage('&#128545;');}}>
            <Typography variant='h2'>&#128545;</Typography>
          </Button>
          <Button onClick={() => {sendMessage('&#128540;');}}>
            <Typography variant='h2'>&#128540;</Typography>
          </Button>
        </DialogContent>
      </Dialog>

      <div className={style.container}>
        <IconButton onClick={toggleDialog} className={style.button} variant='contained' color='primary'>
          <ChatRoundedIcon fontSize={'large'}/>
        </IconButton>
      </div>
    </React.Fragment>
  );
};

export default Chat;
