import React from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import PropTypes from 'prop-types';
import {useSelector} from 'react-redux';
import {useDispatch} from 'react-redux';
import ReactHtmlParser from 'react-html-parser';

import style from './css/OptionSelector.module.css';
import categoriesNames from '../common/categories';
import { wsSelectOption } from '../../features/websocketSlice';
import { selectPlayersByOption } from '../../features/playersSlice';


const categoryToName = (id) => {
  return Object.keys(categoriesNames).filter(name => categoriesNames[name] == id);
};

const OptionSelector = (props) => {

  const selected = useSelector(state => state.game.selected);
  const selectedAns = useSelector(state => state.game.selectedAns);
  const playerId = useSelector(state => state.websocket.playerId);
  const time = useSelector(state => state.game.time);
  const votingCategory = useSelector(state => state.game.categoryVote);
  const playersWithOption = useSelector((state) => selectPlayersByOption(state, props.index));

  const dispatch = useDispatch();

  const avatars = playersWithOption.map((p) => <Avatar className={style.avatar} key={p.playerId} src={`/player${p.avatar}.png`}/>);

  const buttonClass = time > 0 ? style.buttonEnabled : style.buttonDisabled;
  return (<div key={props.index}>
    <div className={style.option}>
      {avatars}
    </div>
    <Button className={buttonClass}
      color={props.index == selectedAns ? 'secondary' : 'primary'}
      size='large'
      aria-label={`answer${props.index}`}
      variant={ props.index == selected ? 'contained' :'outlined'}
      onClick={() => {dispatch(wsSelectOption({
        playerId,
        option: props.index
      }));} }
      name={props.ansButtonName} >
      {ReactHtmlParser(votingCategory ? categoryToName(props.option) : props.option)}
    </Button>

  </div>);
};
OptionSelector.propTypes = {
  ansButtonName: PropTypes.string.isRequired,
  index: PropTypes.number.isRequired,
  option: PropTypes.string.isRequired
};

export default OptionSelector;
