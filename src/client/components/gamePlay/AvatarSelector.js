import React from 'react';
import PropTypes from 'prop-types';
import Paper from '@material-ui/core/Paper';
import Box from '@material-ui/core/Box';
import {makeStyles} from '@material-ui/core/styles';
import {useDispatch} from 'react-redux';

import { wsSelectAvatar } from '../../features/websocketSlice';

const useStyles = makeStyles(() => ({
  selector: {
    margin: '10px',
  },
  used: {
    opacity: '30%'
  },
  selected: {
    borderRadius: '20%',

  }
}));

const AvatarSelector = (props) => {
  const classes = useStyles();
  const dispatch = useDispatch();

  const selectAvatar = () => {
    if (!props.used) {
      dispatch(wsSelectAvatar({
        playerId: props.playerId,
        avatar: props.index
      }));
      props.callback();
    }
  };

  const index = props.index;
  const selected = props.selected;
  return (
    <Box>
      <Paper aria-label='avatar-selector' onClick={selectAvatar} elevation={selected ? 20 : 0}
        className={`${classes.selector} ${selected ? classes.selected : ''}
         ${props.used ? classes.used : ''}`}>
        <img src={`/player${index}.png`}/>
      </Paper>
    </Box>
  );
};

AvatarSelector.propTypes = {
  index: PropTypes.number.isRequired,
  playerId: PropTypes.string,
  selected: PropTypes.bool,
  used: PropTypes.bool,
  callback: PropTypes.func.isRequired
};
export default AvatarSelector;
