import React from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {Typography} from '@material-ui/core';
import CheckIcon from '@material-ui/icons/Check';
import ToggleButton from '@material-ui/lab/ToggleButton';

import {wsSetReady} from '../../features/websocketSlice';
import { selectOwnPlayer } from '../../features/playersSlice';

const ReadyToggle = () => {

  const playerId = useSelector(state => state.websocket.playerId);
  const player = useSelector(selectOwnPlayer);
  const dispatch = useDispatch();

  if (player == null) {
    return null;
  }

  return (
    <React.Fragment>

      <Typography align='center' variant='h3'>Are you ready?</Typography>
      <Typography align='center' variant='h6'>Click below when you are:</Typography>
      <ToggleButton
        value="check"
        selected={player.ready}
        onChange={() => {
          if (player.ready) {
            return false;
          }
          dispatch(wsSetReady({
            playerId,
            ready: !player.ready
          }));
        }}
      >
        <CheckIcon color='primary' />
      </ToggleButton>
      <Typography align='center' variant='h5'>Game will start once all players are ready.</Typography>

    </React.Fragment>
  );
};

export default ReadyToggle;
