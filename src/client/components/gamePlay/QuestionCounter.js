import React from 'react';
import { useSelector} from 'react-redux';
import { Typography } from '@material-ui/core';
import {makeStyles} from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';

const useStyles = makeStyles(() => ({
  timeBox: {
    height: '5vh',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center'
  },
  timer: {
    marginLeft: '50px',
    width: '10px'
  },
  normal: {
    color: '#087108'
  },
  ending: {
    color: '#f33d3d'
  }
}));

const QuestionCounter = () => {
  const classes = useStyles();

  const questionCounter = useSelector(state => state.game.questionCounter);
  const totalQuestions = useSelector(state => state.game.questionNo);

  const counter = questionCounter > 0 ? <Typography color='primary' variant='h4'>Q: {questionCounter}/{totalQuestions}</Typography> : null;
  return (
    <Paper className={classes.timeBox}>
      {counter}
    </Paper>
  );
};

export default QuestionCounter;
