import { describe, expect } from '@jest/globals';
import React from 'react';
import { render, screen } from '@testing-library/react';

import store from '../../app/store';
import Questions from './Questions';
import addProvider from '../../utils/addProvider';
import { setGameState } from '../../features/gameSlice';

const setGame = () => {

  store.dispatch(setGameState({state: {
    question: 'questionsText',
    options: [9, 10, 11, 12],
    categoryVote: true,
    time: 10
  }}));
};

beforeEach(() => {
  setGame();
});

describe('Questions component', () => {
  test('renders Questions component', () => {
    render(addProvider(<Questions playerId={0}/>, store));

    expect(screen.getByRole('button', {name: 'answer0'})).toBeInTheDocument();
    expect(screen.getByRole('button', {name: 'answer1'})).toBeInTheDocument();
    expect(screen.getByRole('button', {name: 'answer2'})).toBeInTheDocument();
    expect(screen.getByRole('button', {name: 'answer3'})).toBeInTheDocument();
    expect(screen.getByText('questionsText')).toBeInTheDocument();
    expect(screen.getByText('Film')).toBeInTheDocument();
    expect(screen.getByText('Books')).toBeInTheDocument();

  });

});
