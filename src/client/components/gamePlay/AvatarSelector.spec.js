import { describe, expect } from '@jest/globals';
import React from 'react';
import {fireEvent, render, screen} from '@testing-library/react';
import configureStore from 'redux-mock-store';

import AvatarSelector from './AvatarSelector';
import addProvider from '../../utils/addProvider';


const mockStore = configureStore();

let store;

beforeEach(() => {
  const initialState = {};
  store = mockStore(initialState);
});

describe('AvatarSelector component', () => {
  test('renders unavailable AvatarSelector component', () => {
    const callback = jest.fn();
    render(addProvider(<AvatarSelector used={true} callback={callback} index={0}/>, store));
    expect(screen.getByRole('img')).toHaveAttribute('src', '/player0.png');
    expect(screen.getByLabelText('avatar-selector').getAttribute('class')).toContain('MuiPaper-elevation0');
    expect(screen.getByLabelText('avatar-selector').getAttribute('class')).toContain('makeStyles-used-2');
  });

  test('renders selected AvatarSelector', () => {
    const callback = jest.fn();
    render(addProvider(<AvatarSelector selected={true} callback={callback} index={0}/>, store));
    expect(screen.getByRole('img')).toHaveAttribute('src', '/player0.png');
    expect(screen.getByLabelText('avatar-selector').getAttribute('class')).toContain('MuiPaper-elevation20');
  });

  test('selects avatar when clicked', () => {
    const callback = jest.fn();
    const playerId = 8;
    const index = 5;
    render(addProvider(<AvatarSelector playerId={playerId.toString()} callback={callback} index={index}/>, store));
    expect(screen.getByRole('img')).toHaveAttribute('src', '/player5.png');

    fireEvent.click(screen.getByRole('img', ));
    const actions = store.getActions();


    expect(callback).toHaveBeenCalled();
    expect(actions).toEqual([{type: 'websocket/wsSelectAvatar',
      payload: {playerId: playerId.toString(), avatar: index }}]);
  });

});
