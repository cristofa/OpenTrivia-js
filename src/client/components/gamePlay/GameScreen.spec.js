import { describe, expect } from '@jest/globals';
import React from 'react';
import { render, screen} from '@testing-library/react';

import store from '../../app/store';
import GameScreen from './GameScreen';
import addProvider from '../../utils/addProvider';
import {updatePlayers} from '../../features/playersSlice';

const defaultName = 'mockName1';

const setPlayer = (player = {}) => {

  const defaultPlayer = {
    playerId:0,
    status:'online',
    name:defaultName,
    avatar:1,
    points:0,
    ready:true,
    selected:null,
    self:false
  };

  store.dispatch(updatePlayers({state: {
    players: [
      {...defaultPlayer, ...player}
    ]
  }}));
};

describe('GameScreen component', () => {
  test('renders GameScreen component', () => {
    setPlayer();
    render(addProvider(<GameScreen/>, store));

    expect(screen.getByRole('img')).toHaveAttribute('src', '/player1.png');
  });


});
