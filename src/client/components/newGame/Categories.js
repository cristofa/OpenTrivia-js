import React from 'react';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';

const useStyles = makeStyles(() => ({
  gridItem: {
    float: 'right',
    textAlign: 'right',
    padding: '0px',
  },
  header: {
    marginLeft: '25px',
    marginBottom: '10px',
  },
  root: {
    marginTop: '5px',
  },
}));


const Categories = (props) => {
  const classes = useStyles();

  const checkboxes = Object.keys(props.categories).map(
    c => (
      <Grid key={c} item xs={6} sm={4} lg={3} className={classes.gridItem}>
        <FormControlLabel
          labelPlacement="start"
          label={c}
          key={c}
          control={(
            <Checkbox
              key={c}
              onChange={() => { props.onChange(c); }}
              checked={props.categories[c]}
              color="primary"
              name={c}
            />
          )}
        />
      </Grid>
    ),
  );

  return (
    <div className={classes.root}>
      <Typography className={classes.header} variant="h5">Categories</Typography>
      <Grid container spacing={1}>
        {checkboxes}
      </Grid>
    </div>
  );
};

Categories.propTypes = {
  onChange: PropTypes.func.isRequired,
  categories: PropTypes.object.isRequired
};

export default Categories;
