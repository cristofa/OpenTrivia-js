import React from 'react';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import FormLabel from '@material-ui/core/FormLabel';
import PropTypes from 'prop-types';

const useStyles = makeStyles(() => ({
  header: {
    marginBottom: '10px',
    display: 'inline',
  },
  root: {
    margin: '3px',
    float: 'left',
    display: 'flex',
    marginTop: '10px',
    marginLeft: '35px',
    justifyContent: 'space-between',

  },
  checkboxes: {
    paddingRight: '15vw',
  },
}));


const Difficulties = (props) => {
  const classes = useStyles();

  const checkboxes = Object.keys(props.difficulties).map(
    d => (
      <FormControlLabel
        labelPlacement="start"
        label={d}
        key={d}
        control={(
          <Checkbox
            key={d}
            onChange={() => { props.onChange(d); }}
            checked={props.difficulties[d]}
            color="primary"
            name={d}
          />
        )}
      />
    ),
  );

  return (
    <div className={classes.root}>
      <FormLabel><Typography className={classes.header} variant="h5">Difficulty</Typography></FormLabel>
      <div className={classes.checkboxes}>
        {checkboxes}
      </div>
    </div>
  );
};

Difficulties.propTypes = {
  difficulties: PropTypes.object.isRequired,
  onChange: PropTypes.func.isRequired
};

export default Difficulties;
