import { describe, expect } from '@jest/globals';
import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import { FetchMock, fetchMock } from '@react-mock/fetch';
import { Router } from 'react-router-dom';
import { createMemoryHistory } from 'history';

import sleep from '../../utils/sleep';
import addRouter from '../../utils/addRouter';
import CreateGame from './CreateGame';
import categoriesNames from '../common/categories';
import { getHttpUrl } from '../../utils/getServerUrl';


describe('CreateGame component', () => {
  test('renders CreateGame component', () => {
    render(addRouter(<CreateGame/>));

    expect(screen.getByRole('button', {name: 'create-game'})).toBeInTheDocument();
    expect(screen.getByRole('slider', {name: 'answer-time'})).toBeInTheDocument();
    expect(screen.getByRole('slider', {name: 'questions-number'})).toBeInTheDocument();

    expect(screen.getByText('Number of questions')).toBeInTheDocument();
    expect(screen.getByText('Answer time')).toBeInTheDocument();
    expect(screen.getByText('Difficulty')).toBeInTheDocument();
    expect(screen.getByText('Categories')).toBeInTheDocument();

    expect(screen.getByText('Easy')).toBeInTheDocument();
    expect(screen.getByText('Medium')).toBeInTheDocument();
    expect(screen.getByText('Hard')).toBeInTheDocument();


    expect(screen.getByRole('checkbox', {name: 'Books'})).toBeInTheDocument();
    expect(screen.getByRole('checkbox', {name: 'Computers'})).toBeInTheDocument();
    expect(screen.getByRole('checkbox', {name: 'Video Games'})).toBeInTheDocument();
    expect(screen.getByRole('checkbox', {name: 'Mythology'})).toBeInTheDocument();


  });

  test('Selects default values and submits', async (done) => {

    const history = createMemoryHistory();
    history.push = jest.fn();

    render(<Router history={history}>
      <FetchMock matcher={getHttpUrl('api/games')}
        response={{uuid: 'test-uuid'}}>
        <CreateGame/>
      </FetchMock>
    </Router>);

    fireEvent.click(screen.getByRole('button', {name: 'create-game'}));

    const [, { body }] = fetchMock.lastCall(getHttpUrl('api/games'), 'POST', 'POST');
    const response = JSON.parse(body);
    expect(response.answerTime).toEqual(10);
    expect(response.questionsNo).toEqual(30);
    expect(response.difficulties).toContain('Easy');
    expect(response.difficulties).toContain('Medium');
    expect(response.difficulties).toContain('Hard');
    Object.keys(categoriesNames).forEach((c) => {
      expect(response.categories).toContain(categoriesNames[c]);
    });

    expect(screen.getByRole('button', {name: 'create-game'})).toBeDisabled();
    expect(screen.getByRole('progressbar')).toBeInTheDocument();
    await sleep(10);
    expect(history.push).toHaveBeenCalledWith({pathname: '/share',
      data:  {
        host: 'http://localhost',
        uuid: 'test-uuid',
      }});
    done();
  });

  test('Submitted values reflect form changes', () => {
    render(addRouter(
      <FetchMock matcher={getHttpUrl('api/games')}
        response={{uuid: 'test-uuid'}}>
        <CreateGame/>
      </FetchMock>));

    const excludeCategory = Object.keys(categoriesNames)[0];
    fireEvent.mouseDown(screen.getByRole('slider', {name: 'questions-number'}), { clientX: 5, clientY: 5 });
    fireEvent.mouseDown(screen.getByRole('slider', {name: 'answer-time'}), { clientX: 5, clientY: 5 });
    fireEvent.click(screen.getByRole('checkbox', {name: 'Hard'}));
    fireEvent.click(screen.getByRole('checkbox', {name: excludeCategory}));

    fireEvent.click(screen.getByRole('button', {name: 'create-game'}));

    const [, { body }] = fetchMock.lastCall(getHttpUrl('api/games'), 'POST');
    const response = JSON.parse(body);
    expect(response.answerTime).toEqual(60);
    expect(response.questionsNo).toEqual(90);
    expect(response.difficulties).toContain('Easy');
    expect(response.difficulties).toContain('Medium');
    expect(response.difficulties).not.toContain('Hard');
    Object.keys(categoriesNames).forEach((c) => {
      if (c === excludeCategory) {
        expect(response.categories).not.toContain(categoriesNames[c]);
      } else {
        expect(response.categories).toContain(categoriesNames[c]);
      }

    });
  });

  test('Submit disabled when no difficulty is selected', () => {
    render(addRouter(<CreateGame/>));
    expect(screen.getByRole('button', {name: 'create-game'})).toBeEnabled();

    fireEvent.click(screen.getByRole('checkbox', {name: 'Hard'}));
    fireEvent.click(screen.getByRole('checkbox', {name: 'Medium'}));
    fireEvent.click(screen.getByRole('checkbox', {name: 'Easy'}));

    expect(screen.getByText('You must select at least one difficulty')).toBeInTheDocument();
    expect(screen.getByRole('button', {name: 'create-game'})).toBeDisabled();

  });

  test('Submit disabled when less then four categories selected', () => {
    render(addRouter(<CreateGame/>));
    expect(screen.getByRole('button', {name: 'create-game'})).toBeEnabled();

    Object.keys(categoriesNames).forEach((c) => {
      fireEvent.click(screen.getByRole('checkbox', {name: c}));
    });
    expect(screen.getByText('You must select at least four categories')).toBeInTheDocument();

    expect(screen.getByRole('button', {name: 'create-game'})).toBeDisabled();
    fireEvent.click(screen.getByRole('checkbox', {name: Object.keys(categoriesNames)[0]}));
    expect(screen.getByRole('button', {name: 'create-game'})).toBeDisabled();
    fireEvent.click(screen.getByRole('checkbox', {name: Object.keys(categoriesNames)[1]}));
    expect(screen.getByRole('button', {name: 'create-game'})).toBeDisabled();
    fireEvent.click(screen.getByRole('checkbox', {name: Object.keys(categoriesNames)[2]}));
    expect(screen.getByRole('button', {name: 'create-game'})).toBeDisabled();
    fireEvent.click(screen.getByRole('checkbox', {name: Object.keys(categoriesNames)[3]}));
    expect(screen.getByRole('button', {name: 'create-game'})).toBeEnabled();

  });

});
