import React from 'react';
import Slider from '@material-ui/core/Slider';
import { withStyles } from '@material-ui/core/styles';
import { withRouter } from 'react-router-dom';
import Paper from '@material-ui/core/Paper';
import FormGroup from '@material-ui/core/FormGroup';
import PlayArrowIcon from '@material-ui/icons/PlayArrow';
import Fab from '@material-ui/core/Fab';
import FormLabel from '@material-ui/core/FormLabel';
import Typography from '@material-ui/core/Typography';
import PropTypes from 'prop-types';

import categoriesNames from '../common/categories';
import theme from '../../theme';
import Categories from './Categories';
import Difficulties from './Difficulties';
import ProgressOverlay from './ProgressOverlay';
import { getHttpUrl } from '../../utils/getServerUrl';
import ErrorDialog from '../common/ErrorDialog';

const styles = () => ({
  slider: {
    width: '60%',
    minWidth: '250px',
    maxWidth: '500px',
    margin: '10px',
    paddingTop: '20px',
    paddingBottom: '35px',
    [theme.breakpoints.up('sm')]: {
      marginRight: '25px',
    },
  },
  label: {
    [theme.breakpoints.up('sm')]: {
      marginLeft: '25px',
      height: '35',
    },
    [theme.breakpoints.down('sm')]: {
      height: '53px',
    },
  },
  submit: {
    float: 'right',
    margin: '10px',
  },
  formGroup: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    margin: '10px',
    maxWidth: '900px',
    [theme.breakpoints.up('sm')]: {
      justifyContent: 'space-between',
    },
    [theme.breakpoints.down('sm')]: {
      justifyContent: 'space-around',
    },
  },
  error: {
    paddingLeft: '15px',
  },
});

const difficulty = ['Easy', 'Medium', 'Hard'];

class CreateGame extends React.Component {
  constructor(props) {
    super(props);

    const categories = {};
    Object.keys(categoriesNames).map((c) => { categories[c] = true; });
    const difficulties = {};
    difficulty.map((d) => { difficulties[d] = true; });

    this.state = {
      questionsNo: 30, answerTime: 10, categories, difficulties, errors: {}, isSubmitting: false, submitError: false
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleQuestionsNo = this.handleQuestionsNo.bind(this);
    this.handleAnswerTime = this.handleAnswerTime.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.toggleCategory = this.toggleCategory.bind(this);
    this.toggleDifficulty = this.toggleDifficulty.bind(this);
  }

  handleChange(event) {
    this.setState({ value: event.target.value });
  }

  handleQuestionsNo(event, newValue) {
    this.setState({ questionsNo: newValue });
  }

  handleAnswerTime(event, newValue) {
    this.setState({ answerTime: newValue });
  }

  toggleCategory(category) {
    const newCategories = { ...this.state.categories };
    newCategories[category] = !this.state.categories[category];
    const errors = this.state.errors;
    const selected = this.filterSelected(newCategories);
    if (selected.length < 4) {
      errors.categories = 'You must select at least four categories';
    } else {
      delete this.state.errors.categories;
    }
    this.setState({
      questionsNo: this.state.questionsNo,
      categories: {
        ...newCategories,
      },
      errors
    });


  }

  toggleDifficulty(diff) {
    const newDifficulties = { ...this.state.difficulties };
    newDifficulties[diff] = !this.state.difficulties[diff];
    const selected = this.filterSelected(newDifficulties);
    const errors = this.state.errors;
    if (selected.length === 0) {
      errors.difficulties = 'You must select at least one difficulty';
    } else {
      delete errors.difficulties;
    }
    this.setState({
      questionsNo: this.state.questionsNo,
      difficulties: {
        ...newDifficulties,
      },
      errors
    });

  }

  filterSelected(map) {
    return Object.keys(map).filter(k => map[k] === true);
  }

  handleSubmit(event) {
    event.preventDefault();
    this.setState({
      ...this.state,
      isSubmitting: true,
    });
    const categories = this.filterSelected(this.state.categories).map(c => categoriesNames[c]);
    const difficulties = this.filterSelected(this.state.difficulties);
    fetch(getHttpUrl('api/games'), {
      method: 'POST',
      body: JSON.stringify({
        questionsNo: this.state.questionsNo,
        answerTime: this.state.answerTime,
        categories,
        difficulties,
      }),
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
      },
    })
      .then(response => {
        if (response.status == 200) {
          return response.json();
        }

        this.setState({
          ...this.state,
          submitError: true
        });
      })
      .then((json) => {
        if (json) {
          const {location} = window;
          let host =  `${location.protocol}//${location.hostname}`;
          if (location.port) {
            host = `${host}:${location.port}`;
          }
          this.props.history.push({
            pathname: '/share',
            data: {
              uuid: json.uuid,
              host,
            },
          });
        }
      });
  }

  render() {
    const { classes } = this.props;

    const marks = [
      { value: 10, label: '10' },
      { value: 30, label: '30' },
      { value: 50, label: '50' },
      { value: 70, label: '70' },
      { value: 90, label: '90' },
    ];

    const marksTime = [
      { value: 10, label: '10s' },
      { value: 20, label: '20s' },
      { value: 30, label: '30s' },
      { value: 40, label: '40s' },
      { value: 50, label: '50s' },
      { value: 60, label: '60s' },
    ];

    const progress = this.state.isSubmitting ? <ProgressOverlay /> : <div />;
    return (
      <React.Fragment>
        {progress}
        <ErrorDialog serverError={this.state.submitError}/>
        <form onSubmit={this.handleSubmit}>
          <Paper>
            <FormGroup className={classes.formGroup}>
              <FormLabel className={classes.label}>
                <Typography variant='h5'>Number of questions</Typography>
              </FormLabel>
              <Slider
                className={classes.slider}
                id='questionsNo'
                name='questions-number'
                aria-label='questions-number'
                defaultValue={30}
                aria-labelledby='discrete-slider-custom'
                step={10}
                onChange={this.handleQuestionsNo}
                valueLabelDisplay='on'
                min={10}
                max={90}
                marks={marks}
              />
            </FormGroup>
          </Paper>
          <Paper>
            <FormGroup className={classes.formGroup}>
              <FormLabel className={classes.label}>
                <Typography variant='h5'>Answer time</Typography>
              </FormLabel>
              <Slider
                className={classes.slider}
                id='answerTime'
                name='answer-time'
                aria-label='answer-time'
                defaultValue={10}
                aria-labelledby='discrete-slider-custom'
                step={10}
                onChange={this.handleAnswerTime}
                valueLabelDisplay='on'
                min={10}
                max={60}
                marks={marksTime}
              />
            </FormGroup>
          </Paper>
          <Paper>
            <FormGroup>
              <Difficulties onChange={this.toggleDifficulty} difficulties={this.state.difficulties} />
            </FormGroup>
            <Typography className={classes.error} color='error' variant='body1'>{this.state.errors.difficulties}</Typography>
          </Paper>
          <Paper>
            <FormGroup className={classes.formGroup}>
              <Categories onChange={this.toggleCategory} categories={this.state.categories} />
            </FormGroup>
            <Typography className={classes.error} color='error' variant='body1'>{this.state.errors.categories}</Typography>
          </Paper>
          <Fab
            disabled={!!this.state.errors.categories || !!this.state.errors.difficulties || this.state.isSubmitting}
            onClick={this.handleSubmit}
            variant='extended'
            size='medium'
            color='secondary'
            aria-label='create-game'
            className={classes.submit}
          >
            Create game
            <PlayArrowIcon />
          </Fab>
        </form>
      </React.Fragment>
    );
  }
}

CreateGame.propTypes = {
  history: PropTypes.object,
  classes: PropTypes.object
};
export default withStyles(styles)(withRouter(CreateGame));
