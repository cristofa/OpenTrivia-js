import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';

const useStyles = makeStyles(() => ({
  root: {
    position: 'fixed',
    display: 'flex',
    width: '100vw',
    minHeight: '98vh',
    background: 'rgba(0, 0, 0, 0.3)',
    alignContent: 'space-around',
    justifyContent: 'space-around',
    alignItems: 'center',
    top: 0,
    bottom: 0,
  },
}));


const ProgressOverlay = () => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <CircularProgress />
    </div>
  );
};

export default ProgressOverlay;
