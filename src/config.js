
export default {
  port: process.env.SERVER_PORT || 3000,
  clientPort: process.env.CLIENT_PORT || 3000,
  host: process.env.SERVER_HOST || 'localhost',

  maxPlayers: 14
};
