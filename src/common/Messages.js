const Messages  = {
  STATUS: {
    type: 'status',
    CONNECTED_TO_PLAY: 'connectedToPlay',
    JOINED_PLAY: 'joinedPlay',
    SET_UP: 'setUp'
  },
  ERROR: {
    type: 'error',
    NOT_FOUND: 'notFound',
    PLAY_FULL: 'playFull',
    PLAY_ENDED: 'playEnded',
    OTHER: 'other'
  },
  JOIN_PLAY: {
    type: 'joinPlay',
    JOIN: 'join',
    REJOIN: 'rejoin'
  },
  UPDATE: {
    type: 'update',
    REQUEST: 'request',
    PLAYERS_STATE: 'playersState',
    GAME_STATE: 'gameState',
    SELECTION: 'selection'
  },
  CHAT: {
    type: 'chat',
    MESSAGE: 'message'
  },
  ACTION: {
    type: 'action',
    SELECT_AVATAR: 'selectAvatar',
    SET_NAME: 'setName',
    SET_READY: 'setReady',
    SELECT_OPTION: 'selectOption',
    CHAT_MESSAGE: 'chatMessage'
  }
};

export default Messages;
