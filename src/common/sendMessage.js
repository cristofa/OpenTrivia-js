const send = (socket, type, message, data = {}) => {
  socket.send(JSON.stringify({ type: type.type, message, data }));
};

export default send;
