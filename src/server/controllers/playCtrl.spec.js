import {describe} from '@jest/globals';
import request from 'supertest';

import app, {wsServer} from '../server';
import send from '../../common/sendMessage';
import Messages from '../../common/Messages';

let httpServer;
let client;

beforeAll(async () => {
  httpServer = app.listen(9999);
  httpServer.on('upgrade', (request, socket, head) => {
    wsServer.handleUpgrade(request, socket, head, socket => {
      wsServer.emit('connection', socket, request);
    });
  });

});

const getClient = async (uuid) => {
  const client = new WebSocket(`ws://localhost:9999/ws/play/${uuid}`);
  await new Promise(resolve => {
    client.onopen = () => {
      resolve();
    };
  });
  return client;
};

const expectMessages = (ws, messages, count) => {
  let promiseResolve;
  const promise = new Promise(resolve => {
    promiseResolve = resolve;
  });

  ws.onmessage = (msg) => {
    messages.push(msg.data);
    if (messages.length == count) {
      promiseResolve();
    }
  };
  return promise;
};

describe('Play controller', () => {
  test('Setup new game', async () => {
    const res = await request(app)
      .post('/api/games')
      .set('Accept', 'application/json')
      .send({
        questionsNo: 30,
        answerTime: 10,
        difficulties: ['Easy', 'Medium', 'Hard'],
        categories: [1, 2, 3, 4]
      });
    expect(res.statusCode).toEqual(200);
    expect(res.body).toHaveProperty('uuid');

    const messages = [];
    client = await getClient(res.body.uuid);
    const messagesPromise = expectMessages(client, messages, 1);

    send(client, Messages.UPDATE, Messages.UPDATE.REQUEST);

    await messagesPromise;
    const state = JSON.parse(messages[0]);
    expect(state.message).toEqual(Messages.UPDATE.PLAYERS_STATE);
    const data = state.data;
    expect(data.players).toEqual([]);
  });
});


afterAll(() => {
  httpServer.close();
});
