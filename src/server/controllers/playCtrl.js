import url from 'url';
import Messages from '../../common/Messages';
import send from '../../common/sendMessage';
import Errors from '../model/Error';

const playController = (wsServer, playService) => {



  wsServer.on('connection', (ws, request) => {
    const path = url.parse(request.url).path;
    const playId = path.split('/')[3];
    const play = playService.getPlay(playId);

    if (play) {
      send(ws, Messages.STATUS, Messages.STATUS.CONNECTED_TO_PLAY, {playId});
      ws.on('message', (msg) => {
        try {
          const message = JSON.parse(msg);
          switch(message.type) {
          case Messages.JOIN_PLAY.type:
            if (message.message == Messages.JOIN_PLAY.JOIN) {
              const playerId = playService.join(playId, ws);
              send(ws, Messages.STATUS, Messages.STATUS.JOINED_PLAY, {playerId});
            } else if (message.message == Messages.JOIN_PLAY.REJOIN) {
              const playerId = message.data.playerId;
              playService.join(playId, ws, playerId);
              const name = playService.getPlayerName(playId, playerId);
              const status = name.length == 0 ? Messages.STATUS.JOINED_PLAY : Messages.STATUS.SET_UP;
              send(ws, Messages.STATUS,  status, {playerId, name});
              playService.broadcastPlayersUpdate(playId);
            }
            break;
          case Messages.UPDATE.type:
            if (message.message == Messages.UPDATE.REQUEST) {
              const state = playService.getPlayerState(playId, message.data.playerId);
              send(ws, Messages.UPDATE,  Messages.UPDATE.PLAYERS_STATE, state);
            }
            break;
          case Messages.ACTION.type:
            if (message.message == Messages.ACTION.SELECT_AVATAR) {
              const success = playService.selectAvatar(playId, message.data.playerId, message.data.avatar);
              if (success) {
                playService.broadcastPlayersUpdate(playId);
              }
            } else if (message.message == Messages.ACTION.SET_NAME) {
              playService.setName(playId, message.data.playerId, message.data.name);
              playService.broadcastPlayersUpdate(playId);
              send(ws, Messages.STATUS,  Messages.STATUS.SET_UP, {playerId: message.data.playerId});
            } else if (message.message == Messages.ACTION.SET_READY) {
              playService.setReady(playId, message.data.playerId, message.data.ready);
              playService.broadcastPlayersUpdate(playId);
            } else if (message.message == Messages.ACTION.SELECT_OPTION) {
              const success = playService.selectOption(playId, message.data.playerId, message.data.option);
              if (success) {
                send(ws, Messages.UPDATE,  Messages.UPDATE.SELECTION, {selected: message.data.option});
              }
            } else if (message.message == Messages.ACTION.CHAT_MESSAGE) {
              playService.broadcastMessage(playId, message.data);
            }
            break;
          }
        } catch (e) {
          switch (e) {
          case Errors.PLAY_FULL:
            send(ws, Messages.ERROR, Messages.ERROR.PLAY_FULL);
            break;
          case Errors.PLAY_ENDED:
            send(ws, Messages.ERROR, Messages.ERROR.PLAY_ENDED);
            break;
          default:
            console.error('Error: ' + e);
            send(ws, Messages.ERROR, Messages.ERROR.OTHER, {error: e});
          }
          ws.close();
        }
      });

      ws.on('close', () => {
        const play = playService.getPlay(playId);
        if (play) {
          playService.disconnect(playId, ws);
          playService.broadcastPlayersUpdate(playId);
        }
      });
    } else {
      send(ws,Messages.ERROR, Messages.ERROR.NOT_FOUND);
      ws.close();
    }

  });
};


export default playController;
