import express from 'express';
import Joi from '@hapi/joi';

import Errors from '../model/Error';

const gamesController = (playService, openTDService) => {
  const controller = express.Router();

  const options = {
    abortEarly: true,
    allowUnknown: false
  };

  controller.post('/', async (req, res) => {
    const schema = Joi.object({
      questionsNo: Joi.number().required().min(10).max(90),
      answerTime: Joi.number().required().min(10).max(60),
      difficulties: Joi.array().required().unique().items(Joi.string().valid('Easy'),Joi.string().valid('Medium'),
        Joi.string().valid('Hard')).min(1),
      categories: Joi.array().required().unique().items(Joi.number()).min(4),
    });
    const { error } = schema.validate(req.body, options);
    if (error) {
      res.status(400).json({ error: error.details[0].message });
      return;
    }

    const token = await openTDService.getToken();
    if (!token) {
      res.status(500);
      res.json({error: Errors.OPENTD_ERROR});
      return;
    }
    const uuid = playService.createPlay(req.body, token);
    res.json({uuid});

  });

  return controller;
};

export default gamesController;
