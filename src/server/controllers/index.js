import playController from './playCtrl';
import gamesController from './gamesCtrl';
import PlayService from '../services/playService';
import OpenTDService from '../services/openTDService';

const setupRouter = (express, wsServer) => {
  const router = express.Router();
  const openTDService = new OpenTDService();
  const playService = new PlayService(openTDService);

  playController(wsServer, playService);
  router.use('/games', gamesController(playService, openTDService));
  return router;
};

export default setupRouter;
