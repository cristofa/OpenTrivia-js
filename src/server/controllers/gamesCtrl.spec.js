import { describe } from '@jest/globals';
import request from 'supertest';

import app from '../server';

describe('Games controller', () => {

  const validateRequest = async (body, status, error = '') => {
    const res = await request(app).post('/api/games').set('Accept', 'application/json').send(body);
    expect(res.statusCode).toEqual(status);
    if (error.length > 0) {
      expect(res.body).toHaveProperty('error');
      expect(res.body.error).toEqual(error);
    }
  };

  test('Validate question number', async (done) => {
    const body = {
      answerTime: 10,
      difficulties: ['soft'],
      categories: [1, 2, 3, 4]
    };
    await validateRequest(body, 400, '"questionsNo" is required');
    body.questionsNo = 5;
    await validateRequest(body, 400, '"questionsNo" must be larger than or equal to 10');
    body.questionsNo = 91;
    await validateRequest(body, 400, '"questionsNo" must be less than or equal to 90');

    done();
  });

  test('Validate answer time', async (done) => {
    const body = {
      questionsNo: 10,
      difficulties: ['soft'],
      categories: [1, 2, 3, 4]
    };
    await validateRequest(body, 400, '"answerTime" is required');
    body.answerTime = 5;
    await validateRequest(body, 400, '"answerTime" must be larger than or equal to 10');
    body.answerTime = 61;
    await validateRequest(body, 400, '"answerTime" must be less than or equal to 60');

    done();
  });

  test('Validate difficulties', async (done) => {
    const body = {
      answerTime: 10,
      questionsNo: 10,
      categories: [1, 2, 3, 4]
    };
    await validateRequest(body, 400, '"difficulties" is required');
    body.difficulties = 'soft';
    await validateRequest(body, 400, '"difficulties" must be an array');
    body.difficulties = [];
    await validateRequest(body, 400, '"difficulties" must contain at least 1 items');
    body.difficulties = ['soft'];
    await validateRequest(body, 400, '"difficulties[0]" does not match any of the allowed types');

    done();
  });

  test('Validate categories', async (done) => {
    const body = {
      answerTime: 10,
      questionsNo: 10,
      difficulties: ['Easy', 'Medium', 'Hard']
    };
    await validateRequest(body, 400, '"categories" is required');
    body.categories = 'Sports';
    await validateRequest(body, 400, '"categories" must be an array');
    body.categories = [2, 3];
    await validateRequest(body, 400, '"categories" must contain at least 4 items');
    body.categories = ['soft'];
    await validateRequest(body, 400, '"categories[0]" must be a number');

    done();
  });

});

