import { v4 as uuidv4 } from 'uuid';
import schedule from 'node-schedule';

import CONST from '../model/const';
import Play from '../model/Play';
import send from '../../common/sendMessage';
import Messages from '../../common/Messages';
import PlayerStatus from '../model/PlayerStatus';
import sleep from '../../client/utils/sleep';
import Errors from '../model/Error';

class PlayService {

  _plays = {};

  constructor(openTDService) {
    this.openTDService = openTDService;

    const rule = new schedule.RecurrenceRule();
    rule.minute = 0;

    schedule.scheduleJob(rule, () => {
      const now = new Date();
      console.log(`Starting cleanup at: ${now.toLocaleString()}`);
      Object.keys(this._plays).forEach((p) => {
        if (this._plays[p].isExpired()) {
          console.log('Deleting play: ' + p);
          delete this._plays[p];
        }
      });
      console.log(Object.keys(this._plays).length + ' active plays left');
    });
  }

  createPlay(config, token) {
    const uuid = uuidv4();
    this._plays[uuid] = new Play(uuid, config.questionsNo, config.answerTime, config.difficulties, config.categories, token);
    return uuid;
  }

  getPlay(playId) {
    return this._plays[playId];
  }

  broadcastGameUpdate = (playId) => {
    const sockets = this.getSockets(playId);
    const state = this.getPlay(playId).getState();
    Object.keys(sockets).forEach(playerId => {
      send(sockets[playerId], Messages.UPDATE,  Messages.UPDATE.GAME_STATE, state);
    });
  };

  broadcastGameError = (playId, e) => {
    console.error('Game error: ' + e);
    const sockets = this.getSockets(playId);
    Object.keys(sockets).forEach(playerId => {
      send(sockets[playerId], Messages.ERROR,  Messages.ERROR.OTHER, e);
    });
  }

  broadcastPlayersUpdate = (playId) => {
    const sockets = this.getSockets(playId);
    Object.keys(sockets).forEach(playerId => {
      const state = this.getPlayerState(playId, playerId);
      send(sockets[playerId], Messages.UPDATE,  Messages.UPDATE.PLAYERS_STATE, state);
    });
  };

  broadcastMessage = (playId, data) => {
    const publicId = this.getPlay(playId).getPlayer(data.playerId).publicId;
    const sockets = this.getSockets(playId);
    Object.keys(sockets).forEach(playerId => {
      send(sockets[playerId], Messages.CHAT,  Messages.CHAT.MESSAGE, {senderId: publicId, message: data.message});
    });
  };

  join(playId, socket, playerId = uuidv4()) {
    return this.getPlay(playId).addPlayer(socket, playerId);
  }

  remove(playId, playerId) {
    this.getPlay(playId).disconnectPlayer(playerId);
  }

  getPlayerName(playId, playerId) {
    return this.getPlay(playId).getPlayerName(playerId);
  }

  getPlayerState(playId, playerId) {
    const state = {
      players: this.getPlay(playId).getPlayers()
    };
    state.players = Object.keys(state.players).map((key) => {
      const playerDTO = { ...state.players[key] };
      delete playerDTO.socket;
      playerDTO.self = playerDTO.playerId === playerId;
      playerDTO.playerId = playerDTO.publicId;
      return playerDTO;
    });
    return state;
  }

  selectAvatar(playId, playerId, avatar) {
    let used = false;
    Object.keys(this.getPlay(playId).getPlayers()).forEach(pId => {
      if (this.getPlay(playId).getPlayer(pId).avatar == avatar) {
        used = true;
      }
    });

    if (used) {
      return false;
    }
    this.getPlay(playId).getPlayer(playerId).avatar = avatar;
    return true;
  }

  setName(playId, playerId, name) {
    this.getPlay(playId).getPlayer(playerId).name = name;
  }

  selectOption(playId, playerId, option) {
    const play = this.getPlay(playId);
    const result = play.selectOption(playerId, option);

    /* if (play.isVotingCategory()) {
      let allSelected = true;
      Object.keys(play.getPlayers()).forEach(pId => {
        const player = play.getPlayer(pId);
        if (player.selected !== null) {
          allSelected = false;
          console.log("ALL SELETED1: false" + allSelected + player.selected);
        }
      });

      console.log("ALL SELETED2: " + allSelected);
      if (allSelected) {
        play.clearInterval();
        play.setTime(0);
        const callback = play.getCallback();
        callback(play);
      }
    }*/

    return result;
  }

  setReady(playId, playerId, ready) {
    const play = this.getPlay(playId);
    if (play.getIsRunning()) {
      return;
    }
    play.getPlayer(playerId).ready = ready;
    play.setTime(CONST.READY_WAIT_TIME);
    Object.keys(play.getPlayers()).forEach(pId => {
      const player = play.getPlayer(pId);
      if (player.status == PlayerStatus.ONLINE && !player.ready) {
        play.setTime(null);
        play.clearInterval();
        this.broadcastGameUpdate(playId);
        return;
      }
    });

    if (play.getTime()) {
      this.startTimerInPlay(play, this.voteCategory.bind(this));
    }
  }

  voteCategory(play) {
    play.setIsRunning(true);
    play.prepareCategoryVote();
    this.broadcastGameUpdate(play.id);
    this.broadcastPlayersUpdate(play.id);
    this.startTimerInPlay(play, this.showCategory.bind(this));
  }

  async showCategory(play) {
    this.broadcastPlayersUpdate(play.id);
    await sleep(3000);
    play.selectCategory();
    this.broadcastGameUpdate(play.id);
    await sleep(2000);
    this.poseQuestion(play);
  }

  poseQuestion(play) {
    this.openTDService.getQuestion(play.getCategory(), play.getDifficulty(), play.getToken())
      .then(question => {
        if (question == null) {
          this.broadcastGameError(play.id, Errors.OPENTD_ERROR);
          return;
        }
        play.prepareQuestion(question);
        this.broadcastGameUpdate(play.id);
        this.broadcastPlayersUpdate(play.id);
        this.startTimerInPlay(play, this.showAnswer.bind(this));
      }).catch(e =>
      { this.broadcastGameError(play.id, e); }
      );
  }

  async showAnswer(play) {
    this.broadcastPlayersUpdate(play.id);
    await sleep(3000);
    play.selectCorrectAnswer();
    this.broadcastGameUpdate(play.id);
    this.broadcastPlayersUpdate(play.id);
    await sleep(2000);

    if (!play.isFinished()) {
      this.voteCategory(play);
    } else {
      console.log(`Game ${play.id} finished`);
      play.finishGame();
      this.broadcastGameUpdate(play.id);
    }
  }

  startTimerInPlay(play, onFinish) {
    if (play.getInterval()) {
      play.clearInterval();
    }

    play.setCallback(onFinish);
    this.broadcastGameUpdate(play.id);
    const interval = setInterval(() => {
      let time = play.getTime();
      play.setTime(--time);
      if (time < 0) {
        play.clearInterval();
        play.setTime(null);
        play.getCallback()(play);
      }
      this.broadcastGameUpdate(play.id);
    }, 1000);
    play.setInterval(interval);
  }

  getSockets(playId) {
    const sockets = {};
    Object.keys(this.getPlay(playId).getPlayers()).forEach(pId => {
      if (this.getPlay(playId).getPlayer(pId).socket) {
        sockets[pId] = this.getPlay(playId).getPlayer(pId).socket;
      }
    });
    return sockets;
  }

  disconnect(playId, socket) {
    Object.keys(this.getPlay(playId).getPlayers()).forEach(pId => {
      if (this.getPlay(playId).getPlayer(pId).socket == socket) {
        this.getPlay(playId).getPlayer(pId).disconnect();
      }
    });
  }
}

export default PlayService;
