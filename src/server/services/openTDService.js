import fetch from 'node-fetch';


class OpenTDService {

  constructor() {
    this._host = 'https://opentdb.com';

    this._tokenUri = 'api_token.php?command=request';
    this._questionUri = 'api.php?amount=1';
  }

  async getQuestion(category, difficulty, token) {
    let json = await fetch(`${this._host}/${this._questionUri}&category=${category}&difficulty=${difficulty}&token=${token}`)
      .then(res => res.json()).then(json => json);
    if (json.response_code == 4) {
      json = await fetch(`${this._host}/${this._questionUri}&category=${category}&token=${token}`)
        .then(res => res.json()).then(json => json);
    }
    if (json.response_code !== 0) {
      console.log(`${this._host}/${this._questionUri}&category=${category}&difficulty=${difficulty}&token=${token} OpenTD call return code ${json.response_code}`);
      return null;
    }
    return json.results[0];
  }


  async getToken() {
    const json = await fetch(`${this._host}/${this._tokenUri}`).then(res => res.json()).then(json => json);
    if (json.response_code !== 0) {
      return null;
    }
    return json.token;
  }
}

export default OpenTDService;
