import config from '../../config';
import Player from './Player';
import Errors from './Error';
import CONST from './const';

class Play {

  constructor(id, questionsNo, answerTime, difficulties, categories, token) {
    this.id = id;
    this._questionNo = questionsNo;
    this._answerTime = answerTime;
    this._difficuleties = difficulties;
    this._categories = categories;
    this._openTDToken = token;
    this._question = null;
    this._options = [];
    this._selectedAns = null;
    this._interval = null;
    this._time = null;
    this._isRunning = false;
    this._questionCounter = 0;
    this._category = null;
    this._correctAns = null;
    this._categoryVote = false;
    this._ended = false;
    this._maxScore = 0;
    this._timerCallback = null;

    this._created = new Date();
    this._players = {};

  }

  getPlayers() {
    return this._players;
  }

  getPlayer(playerId) {
    return this._players[playerId];
  }

  addPlayer(socket, playerId) {
    if (this.isFinished()) {
      throw Errors.PLAY_ENDED;
    }

    if (this._players[playerId]) {
      this._players[playerId].connect(socket);
      return playerId;
    }

    if (Object.keys(this._players).length < config.maxPlayers) {
      this._players[playerId] = new Player(playerId, Object.keys(this._players).length, socket);
      return playerId;
    } else {
      throw Errors.PLAY_FULL;
    }
  }

  setCallback(callback) {
    this._timerCallback = callback;
  }

  getCallback() {
    return this._timerCallback;
  }

  isVotingCategory() {
    return this._categoryVote;
  }

  setTime(time) {
    this._time = time;
  }

  getTime() {
    return this._time;
  }

  setInterval(interval) {
    this._interval = interval;
  }

  getInterval() {
    return this._interval;
  }

  getIsRunning() {
    return this._isRunning;
  }

  setIsRunning(value) {
    this._isRunning = value;
  }

  clearInterval() {
    clearInterval(this._interval);
    this._interval = null;
  }

  getState() {
    return {
      time: this._time,
      categoryVote: this._categoryVote,
      question: this._question,
      options: this._options,
      isRunning: this._isRunning,
      selectedAns: this._selectedAns,
      questionCounter: this._questionCounter,
      questionNo: this._questionNo,
      ended: this._ended,
      maxScore: this._maxScore
    };
  }

  selectOption(playerId, option) {
    if (this._time <= 0) {
      return false;
    }
    this._players[playerId].selected = option;

    return true;
  }

  prepareQuestion(question) {
    this._time = this._answerTime;
    this._categoryVote = false;
    this._selectedAns = null;
    this._question = question.question;
    this._options = question.incorrect_answers;
    this._correctAns = Math.round(Math.random() * this._options.length);
    this._options.splice(this._correctAns, 0, question.correct_answer);
    this._selectedAns = null;
    Object.keys(this.getPlayers()).forEach(pId => {
      this.getPlayer(pId).selected = null;
    });
  }

  prepareCategoryVote() {
    this._time = CONST.CATEGORY_VOTE_TIME;
    this._categoryVote = true;
    this._selectedAns = null;
    this._questionCounter++;
    this._question = 'Vote for next category';
    let selected = [];
    Object.keys(this.getPlayers()).forEach(pId => {
      this.getPlayer(pId).selected = null;
    });

    if (this._categories.length > 8) {
      while (selected.length != 4) {
        const random = this._categories[Math.floor(Math.random() * this._categories.length)];
        if (selected.indexOf(random) < 0) {
          selected.push(random);
        }
      }
    } else {
      selected = [...this._categories];
      while (selected.length > 4) {
        const random = Math.floor(Math.random() * selected.length);
        selected.splice(random, 1);
      }
    }

    this._options = selected;
  }

  isFinished() {
    return this._questionCounter >= this._questionNo || this._ended;
  }

  isExpired() {
    return (this._ended && this.getOnlinePlayersCount() === 0) || (new Date() - this._created > 1000*60*60*8);
  }

  finishGame() {
    this._ended = true;
    let maxScore = 0;
    Object.keys(this.getPlayers()).forEach(pId => {
      if (this.getPlayer(pId).points > maxScore) {
        maxScore = this.getPlayer(pId).points;
      }
    });
    console.log(`Game finished. Max score was ${maxScore}`);
    this._maxScore = maxScore;
  }

  selectCorrectAnswer() {
    this._selectedAns = this._correctAns;
    Object.keys(this.getPlayers()).forEach(pId => {
      if(this.getPlayer(pId).selected == this._correctAns) {
        this.getPlayer(pId).points++;
      }
    });
  }

  selectCategory() {
    const results = [0, 0, 0, 0];
    Object.keys(this.getPlayers()).forEach(pId => {
      results[this.getPlayer(pId).selected]++;
    });
    let max = 0;
    let maxIndex = [];
    for (let i = 0; i < 4; i++) {
      if (results[i] > max) {
        max = results[i];
        maxIndex = [i];
      } else if (results[i] == max) {
        maxIndex.push(i);
      }
    }
    const randomIndex = Math.floor(Math.random()*maxIndex.length);
    this._selectedAns = maxIndex[randomIndex];
    this._category = this._options[this._selectedAns];
  }

  getCategory() {
    return this._category;
  }

  getToken() {
    return this._openTDToken;
  }

  getDifficulty() {
    const randomIndex = Math.floor(Math.random()*this._difficuleties.length);
    return this._difficuleties[randomIndex].toLowerCase();
  }

  getPlayerName(playerId) {
    return this._players[playerId].name;
  }

  disconnectPlayer(playerId) {
    this._players[playerId].disconnect();
  }

  getOnlinePlayersCount() {
    return Object.keys(this._players).filter(pid => this._players[pid].socket !== null).length;
  }

}

export default Play;
