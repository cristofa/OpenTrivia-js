import PlayerStatus from './PlayerStatus';

class Player {
  constructor(playerId, publicId, socket) {
    this.playerId = playerId;
    this.publicId = publicId;
    this.socket = socket;
    this.status = PlayerStatus.ONLINE;
    this.name = '';
    this.avatar = 0;
    this.points = 0;
    this.ready = false;
    this.selected = null;
  }

  disconnect() {
    this.status = PlayerStatus.OFFLINE;
    this.socket = null;
  }

  connect(socket) {
    this.socket = socket;
    this.status = PlayerStatus.ONLINE;
  }
}

export default Player;
