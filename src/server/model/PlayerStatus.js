const PlayerStatus  = {
  ONLINE: 'online',
  OFFLINE: 'offline'
};

export default PlayerStatus;
