const Errors  = {
  PLAY_FULL: 'playFull',
  PLAY_ENDED: 'playEnded',
  OPENTD_ERROR: 'openTDServiceError'
};

export default Errors;
