import app from './server';
import { wsServer } from './server';

function startServer() {
  return new Promise((resolve, reject) => {

    const httpServer = app.listen(app.get('port'));

    httpServer.on('upgrade', (request, socket, head) => {
      wsServer.handleUpgrade(request, socket, head, socket => {
        wsServer.emit('connection', socket, request);
      });
    });

    httpServer.once('error', (err) => {
      if (err.code === 'EADDRINUSE') {
        reject(err);
      }
    });

    httpServer.once('listening', () => resolve(httpServer));
  }).then((httpServer) => {
    const { port } = httpServer.address();
    console.info(`==> 🌎 Listening on ${port}. Open up http://localhost:${port}/ in your browser.`);

    // Hot Module Replacement API
    if (module.hot) {
      let currentApp = app;
      module.hot.accept('./server', () => {
        httpServer.removeListener('request', currentApp);
        import('./server')
          .then(({ default: nextApp }) => {
            currentApp = nextApp;
            httpServer.on('request', currentApp);
            console.log('HttpServer reloaded!');
          })
          .catch((err) => console.error(err));
      });

      // For reload main module (self). It will be restart http-server.
      module.hot.accept((err) => console.error(err));
      module.hot.dispose(() => {
        console.log('Disposing entry module...');
        httpServer.close();
      });
    }
  });
}

console.log('Starting http server...');
startServer().catch((err) => {
  console.error('Error in server start script.', err);
});
