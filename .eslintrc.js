module.exports = {
  parser: 'babel-eslint',
  parserOptions: {
    ecmaFeatures: {
      jsx: true
    },
    ecmaVersion: 11,
    sourceType: 'module'
  },
  extends: [
    'eslint:recommended',
    'plugin:react/recommended',
    'plugin:import/errors'
  ],
  plugins: ['jest', 'react', 'import'],
  globals: {
    __DEV__: true
  },
  env: {
    browser: true,
    node: true,
    jest: true,
    es6: true
  },

  rules: {
    'indent': [
      'error',
      2
    ],
    'linebreak-style': [
      'error',
      'unix'
    ],
    'quotes': [
      'error',
      'single'
    ],
    'semi': [
      'error',
      'always'
    ],
    'import/order': [
      'error',
      {'newlines-between': 'always',
        'groups': [
          'internal',
          'external'
        ]}]
  }
};
