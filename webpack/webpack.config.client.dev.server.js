const HtmlWebpackPlugin = require('html-webpack-plugin');
const webpack = require('webpack');

const config = require('./webpack.config.base');
const findConfig = require('./utils');

let client = findConfig(config, 'client');

client = {
  ...client,
  output: {
    publicPath: '/',
  },
  plugins: [
    ...client.plugins,
    new webpack.HotModuleReplacementPlugin(),
    new HtmlWebpackPlugin({
      template: 'src/client/index.html',
      filename: 'index.html',
      inject: 'body',
    })
  ],
  devServer: {
    host: '0.0.0.0',
    port: 3001,
    historyApiFallback: true,
    hot: true,
    contentBase: './public',
  },
};

module.exports = client;
